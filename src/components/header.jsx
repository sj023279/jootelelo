import $ from "jquery";
import React, { useContext, useEffect, useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { authContext, headerContext } from "../App";
import LogoWhite from "../images/jootelelo2.png";
// import { loginUser, registerUser, sendOtp, setToken } from "../services/user";
import mensBanner from "../images/Men_banner.webp";
import womenBanner from "../images/Women_Banner.webp";
import { getAllCategory } from "../services/Category";
import { getAllGenders } from "../services/Product";
import {
  loginUser,
  registerUser,
  setToken,
  sendOtp,
  forgotPasswordService,
  checkUserExists,
} from "../services/user";

function Header() {
  const [isAuthorized, setIsAuthorized] = useContext(authContext);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [showDropdown, setShowDropdown] = useState(false);
  const [dropdownType, setDropdownType] = useState("");
  const [toggleHeader, setToggleHeader] = useContext(headerContext);

  const [toggleModalType, setToggleModalType] = useState(1); //1 for login 2 for register 3 for forgot password

  const [categoryArr, setCategoryArr] = useState([]);
  const [genederArr, setGenederArr] = useState([]);
  const [tempOtp, settempOtp] = useState("");
  const [compareOtp, setCompareOtp] = useState("");

  const [registerOtp, setRegisterOtp] = useState("");
  const [compareRegisterOtp, setCompareRegisterOtp] = useState("");

  const navigate = useNavigate();

  const [searchQuery, setSearchQuery] = useState("");
  const [currentScreenWidth, setCurrentScreenWidth] = useState(
    window.innerWidth
  );
  const register_link = () => {
    $(".register_form").addClass("display_block");
    $(".sign_in_form").addClass("display_none");
  };
  const register = async (e) => {
    e.preventDefault();

    try {
      if (password != confirmPassword) {
        alert("Passwords doesn't match");
      } else if (name == "") {
        alert("Please Enter name");
      } else if (!(phone.length <= 10 && phone.length > 0)) {
        alert("Please Enter a valid number");
      } else if (email == "") {
        alert("Please Enter a valid email");
      } else {
        let obj = {
          name,
          email,
          phone,
          password,
        };
        let { data: res } = await registerUser(obj);
        if (res) {
          console.log(res);
          setToggleHeader(false);
          $(".register_form").addClass("display_block");
          $(".sign_in_form").addClass("display_none");
          alert(res.message);
        }
      }
    } catch (err) {
      if (err?.response?.data?.message) {
        $(".register_form").addClass("display_block");
        $(".sign_in_form").addClass("display_none");
        console.log(err?.response?.data?.message);
        alert(err?.response?.data?.message);
      } else {
        console.log(err);
        alert(err);
      }
    }
  };

  const handleOtpSend = async () => {
    try {
      let { data: response } = await checkUserExists({ phone: phone });
      if (response.success) {
        let res = await sendOtp(phone);
        console.log(res, "asd");
        if (res) {
          setRegisterOtp(res.data.data);
          setToggleModalType(4);
        }
      }
    } catch (error) {
      console.error(error);
      if (error.response.data.message) {
        alert(error.response.data.message);
      } else {
        alert(error.message);
      }
    }
  };

  const handleOtpVerify = async () => {
    try {
      if (registerOtp === compareRegisterOtp) {
        setToggleModalType(2);
      } else {
        alert("Please Enter valid otp");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const sign_in = async (e) => {
    e.preventDefault();

    try {
      if (email == "") {
        alert("Please Enter a valid Email");
      } else if (password == "") {
        alert("Please Enter a password");
      } else {
        let tempCart = localStorage.getItem("jootelelo-cart");
        let obj = {
          email,
          password,
          cartArr: JSON.parse(tempCart),
        };
        let { data: res } = await loginUser(obj);
        if (res) {
          console.log(res);
          setToken(res.token);
          setIsAuthorized(true);
          alert(res.message);
        }
      }
    } catch (err) {
      if (err?.response?.data?.message) {
        console.log(err?.response?.data?.message);
        alert(err?.response?.data?.message);
      } else {
        console.log(err);
        alert(err);
      }
    }
  };

  const dropdown = () => {
    $(".dropdown-menu").slideToggle();
  };

  const handleRedirect = (obj) => {
    // $(".dropdown-menu").slideToggle();
    // navigate(`/products?gender=${obj.gender}&category=${obj.category}`);
    navigate(`/products?gender=${obj.gender}`);
  };
  const handleDropdown = (v) => {
    if (currentScreenWidth > 700) {
      setDropdownType(v);
      setShowDropdown(true);
    }
  };

  const getCategories = async () => {
    try {
      const { data: res } = await getAllCategory();
      if (res.success) {
        setCategoryArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const getGender = async () => {
    try {
      const { data: res } = await getAllGenders();
      if (res.success) {
        setGenederArr([
          ...res.data
            .filter((el) => el != "MALE")
            .map(
              (el) =>
                `${el}`.charAt(0).toUpperCase() + `${el}`.slice(1).toLowerCase()
            ),
        ]);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleForgotOtpSend = async () => {
    try {
      const { data: res } = await sendOtp(phone);
      if (res) {
        settempOtp(res.data);
      }
    } catch (error) {
      console.error(error);
      alert(error.message);
    }
  };
  const handleForgotPassword = async () => {
    try {
      if (tempOtp == compareOtp) {
        let obj = {
          phone,
          password,
        };
        let { data: res } = await forgotPasswordService(obj);
        if (res) {
          alert(res.message);
        }
      } else {
        alert("Please Enter a valid OTP");
      }
    } catch (error) {
      console.error(error);
      alert(error.message);
    }
  };

  const [windowwidth, setWidth] = useState(window.innerWidth);
  const updateDimensions = () => {
    console.log(window.innerWidth);
    setWidth(window.innerWidth);
  };
  useEffect(() => {
    window.addEventListener("resize", updateDimensions);
    return () => window.removeEventListener("resize", updateDimensions);
  }, []);

  useEffect(() => {
    getCategories();
    getGender();
  }, []);

  return (
    <header>
      <div className="container-fluid">
        <div className="row">
          {/* desktop navbar */}
          <div className="col-lg-12 custom-navbar px-5 py-4">
            <div className="row d-flex align-items-center justify-content-between">
              <div
                className="col-lg-3 mt-2 d-flex align-items-center justify-content-center"
                onClick={() => navigate("/")}
                style={{ cursor: "pointer" }}
              >
                <img
                  alt=""
                  src={LogoWhite}
                  style={{ height: "50px", width: "120px" }}
                  className="navbar_logo"
                />
              </div>

              <div className="col-5">
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginTop: windowwidth < 600 ? 10 : 0,
                  }}
                >
                  <input
                    type="text"
                    value={searchQuery}
                    onChange={(e) => setSearchQuery(e.target.value)}
                    style={{
                      width: windowwidth < 600 ? "45vw" : "380px",
                      padding: "5px 0px 5px 20px",
                      borderRadius: 5,
                    }}
                    placeholder={
                      windowwidth < 600
                        ? "Search here ..."
                        : "Search Products here ..."
                    }
                  />
                  <div
                    className="btn"
                    style={{
                      backgroundColor: "#516EAE",
                      color: "white",
                      padding: "5px 15px",
                      marginLeft: 15,
                    }}
                    onClick={() => navigate(`/products?search=${searchQuery}`)}
                  >
                    Search
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-12 d-flex align-items-center justify-content-evenly">
                <div className="row">
                  <div className="col-lg-12 col-12 mt-3 mt-lg-0 mt-md-0 d-flex align-items-lg-center justify-content-lg-between">
                    {isAuthorized ? (
                      <i
                        onClick={() => navigate("/account/wishlist")}
                        class="fa-solid fa-heart ms-4"
                        style={{ color: "white", fontSize: 25 }}
                      ></i>
                    ) : (
                      <></>
                    )}
                    <i
                      class="fa-solid fa-cart-arrow-down  ms-4"
                      style={{ color: "white", fontSize: 25 }}
                      onClick={() => navigate("/cart")}
                    ></i>
                    {isAuthorized ? (
                      <i
                        onClick={() => navigate("/account")}
                        class="fa-solid fa-circle-user ms-4"
                        style={{ color: "white", fontSize: 20 }}
                      ></i>
                    ) : (
                      <i
                        onClick={() => setToggleHeader(true)}
                        class="fa-solid fa-circle-user ms-4"
                        style={{ color: "white", fontSize: 25 }}
                      ></i>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12">
            <div
              style={{
                display: "flex",
                flexDirection: windowwidth < 700 ? "column" : "row",
                justifyContent: "space-between",
                width: "70vw",
                marginLeft: "13vw",
                padding: "15px 0px",
              }}
            >
              <div
                style={{ cursor: "pointer" }}
                className=" mt-3 mt-lg-0 mt-md-0 text-center"
                onClick={() => navigate("/")}
              >
                Home
              </div>
              <div
                style={{ cursor: "pointer" }}
                onClick={() => navigate("/about")}
                className="mt-3 mt-lg-0 mt-md-0 text-center"
              >
                About us
              </div>
              {/* <div
                    className="navbar_text mt-3 mt-lg-0 mt-md-0 text-center"
                    onClick={() => handleRedirect({ gender: "MEN" })}
                  >
                    New Arrival
                  </div> */}
              {genederArr.map((el) => {
                return (
                  <div
                    style={{ cursor: "pointer" }}
                    className=" mt-3 mt-lg-0 mt-md-0 text-center"
                    onClick={() => handleRedirect({ gender: el })}
                  >
                    {el}
                  </div>
                );
              })}

              <div
                style={{ cursor: "pointer" }}
                onClick={() => navigate("/contact")}
                className=" mt-3 mt-lg-0 mt-md-0 text-center"
              >
                Contact us
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="top_sec">
        <div className="container">
          {/* <p className="m-0">
            5% OFF ON PREPAID ORDERS.FREE DELIVERY. FREE RETURNS.
          </p> */}
          <div className="login_sec col-5">
            {!isAuthorized && (
              <>
                <div
                  className={`${
                    toggleHeader ? `custom-modal-shown` : `custom-modal-hidden`
                  }  `}
                >
                  <div
                    className="modal-dialog modal-dialog-centered"
                    aria-hidden="true"
                    aria-labelledby="exampleModalLabel"
                  >
                    <div className="modal-content">
                      <div className="text-end">
                        <button
                          type="button"
                          className="btn-close model_close btn-close-white"
                          onClick={() => {
                            setToggleHeader(false);
                            setToggleModalType(1);
                          }}
                        ></button>
                      </div>
                      <div className="modal-body">
                        <div className="col-sm-12">
                          <div className="model_logo_div">
                            <img
                              alt=""
                              src={LogoWhite}
                              className="model_logo"
                            />
                          </div>
                          <div className="model_signin">
                            {toggleModalType == 1 && (
                              <>
                                <p>Great to have you back!</p>
                                <form className="sign_in_form">
                                  <input
                                    className="form-control"
                                    type="email"
                                    name="email"
                                    placeholder="Email address"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                  />
                                  <input
                                    className="form-control"
                                    type="password"
                                    name="password"
                                    placeholder="password"
                                    value={password}
                                    onChange={(e) =>
                                      setPassword(e.target.value)
                                    }
                                  />
                                  <Link
                                    to="#"
                                    className="d-block forgot_password"
                                    data-bs-target="#email-modal"
                                    data-bs-toggle="modal"
                                  >
                                    Forgot Password?
                                  </Link>
                                  <button
                                    className="sign_in"
                                    onClick={(e) => sign_in(e)}
                                  >
                                    Sign In
                                  </button>
                                  <p className="register_now">
                                    Don't have an account?{" "}
                                    <span
                                      onClick={() => setToggleModalType(3)}
                                      className="register_link"
                                    >
                                      {" "}
                                      Register now{" "}
                                    </span>
                                  </p>
                                </form>
                              </>
                            )}
                            {toggleModalType == 2 && (
                              <>
                                <p>Welcome to Joote Lelo!</p>
                                <form className="register_form d-block">
                                  <input
                                    className="form-control"
                                    type="email"
                                    name="email"
                                    placeholder="Email address"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                  />
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="number"
                                    placeholder="Phone"
                                    value={phone}
                                    onChange={(e) => setPhone(e.target.value)}
                                  />
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="phone"
                                    placeholder="Name"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                  />
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="password"
                                    placeholder="Password"
                                    value={password}
                                    onChange={(e) =>
                                      setPassword(e.target.value)
                                    }
                                  />
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="confirmPassword"
                                    placeholder="Confirm Password"
                                    value={confirmPassword}
                                    onChange={(e) =>
                                      setConfirmPassword(e.target.value)
                                    }
                                  />
                                  <button
                                    className="sign_in"
                                    onClick={(e) => register(e)}
                                  >
                                    Sign Up
                                  </button>
                                  <p className="register_now">
                                    Already have an account?{" "}
                                    <span
                                      onClick={() => setToggleModalType(1)}
                                      className="sign_in_link"
                                    >
                                      {" "}
                                      sign in{" "}
                                    </span>
                                  </p>
                                </form>
                              </>
                            )}
                            {toggleModalType == 3 && (
                              <form className="register_form d-block">
                                <input
                                  className="form-control"
                                  type="text"
                                  name="number"
                                  placeholder="Phone"
                                  value={phone}
                                  onChange={(e) => setPhone(e.target.value)}
                                />

                                <button
                                  className="sign_in"
                                  type="button"
                                  onClick={() => handleOtpSend()}
                                >
                                  Send Otp
                                </button>
                                <p className="register_now">
                                  Already have an account?{" "}
                                  <span
                                    onClick={() => setToggleModalType(1)}
                                    className="sign_in_link"
                                  >
                                    {" "}
                                    sign in{" "}
                                  </span>
                                </p>
                              </form>
                            )}
                            {toggleModalType == 4 && (
                              <form className="register_form d-block">
                                <input
                                  className="form-control"
                                  type="text"
                                  name="number"
                                  placeholder="Enter Otp"
                                  value={compareRegisterOtp}
                                  onChange={(e) =>
                                    setCompareRegisterOtp(e.target.value)
                                  }
                                />

                                <button
                                  className="sign_in"
                                  type="button"
                                  onClick={() => handleOtpVerify()}
                                >
                                  Verify Otp
                                </button>
                                <p className="register_now">
                                  Already have an account?{" "}
                                  <span
                                    onClick={() => setToggleModalType(1)}
                                    className="sign_in_link"
                                  >
                                    {" "}
                                    sign in{" "}
                                  </span>
                                </p>
                              </form>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}

            <div
              className="modal fade email-form"
              id="email-modal"
              aria-labelledby="exampleModalLabel2"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="text-end">
                    <button
                      type="button"
                      className="btn-close model_close btn-close-white"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    <div className="col-sm-12">
                      <div className="model_logo_div">
                        <img
                          alt=""
                          src="assets/images/newlogo.png"
                          className="model_logo"
                        />
                      </div>
                      <div className="model_signin">
                        <p>Enter Your Phone Number To Proceed</p>
                        <form className="sign_in_form">
                          <input
                            className="form-control"
                            type="email"
                            name="email2"
                            onChange={(e) => setPhone(e.target.value)}
                            placeholder="Phone"
                          />
                          <button
                            className="btn sign_in"
                            data-bs-target="#pin-modal"
                            data-bs-toggle="modal"
                            onClick={(e) => {
                              handleForgotOtpSend();
                              e.preventDefault();
                            }}
                          >
                            Proceed
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="modal fade pin-form"
              id="pin-modal"
              aria-labelledby="exampleModalLabel3"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="text-end">
                    <button
                      type="button"
                      className="btn-close model_close btn-close-white"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    <div className="col-sm-12">
                      <div className="model_logo_div">
                        <img
                          alt=""
                          src="assets/images/newlogo.png"
                          className="model_logo"
                        />
                      </div>
                      <div className="model_signin">
                        <p>Enter Pin Number That You Received!!</p>
                        <form className="sign_in_form">
                          <input
                            className="form-control"
                            type="text"
                            name="pin"
                            onChange={(e) => setCompareOtp(e.target.value)}
                            placeholder="Pin Number"
                          />
                          <input
                            className="form-control"
                            type="password"
                            name="password3"
                            onChange={(e) => setPassword(e.target.value)}
                            placeholder="Enter Password"
                          />
                          <button
                            className="btn sign_in"
                            // data-bs-target="#password-modal"
                            // data-bs-toggle="modal"
                            onClick={(e) => {
                              handleForgotPassword();
                              e.preventDefault();
                            }}
                          >
                            Proceed
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="modal fade password-form"
              id="password-modal"
              aria-labelledby="exampleModalLabel4"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="text-end">
                    <button
                      type="button"
                      className="btn-close model_close btn-close-white"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    <div className="col-sm-12">
                      <div className="model_logo_div">
                        <img
                          alt=""
                          src="assets/images/newlogo.png"
                          className="model_logo"
                        />
                      </div>
                      <div className="model_signin">
                        <p>Enter your new password</p>
                        <form className="sign_in_form">
                          <input
                            className="form-control"
                            type="password"
                            name="password2"
                            placeholder="Password"
                          />
                          <input
                            className="form-control"
                            type="password"
                            name="password3"
                            placeholder="Comfirm Passcord"
                          />
                          <button className="sign_in">Submit</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
