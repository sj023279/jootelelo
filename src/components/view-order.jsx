import React, { useState, useEffect } from 'react'
import { getOrderById } from '../services/Order';
import PageBanner from './page-banner';

import { useParams } from 'react-router-dom'

function ViewOrder() {

    const [orderObj, setOrderObj] = useState({});

    const params = useParams()
    const getOrder = async () => {
        try {
            const { data: res } = await getOrderById(params.id);
            if (res.success) {
                console.log(res.data)
                setOrderObj(res.data)
            }
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getOrder()
    }, [])

    return (
        <div className='my-order'>
            <PageBanner name='View Order' />

            <section id="view_order">
                <div className="container">
                    <h4>View Order</h4>
                    <div className="row">
                        <div className="col-12 col-md-4">
                            <div className="order-info">
                                <p className='order-subheading'>ORDER INFORMATION</p>
                                <ul>
                                    <li>Phone:<span>{orderObj?.userDetails?.phone}</span></li>
                                    <li>Email: <span>{orderObj?.userDetails?.email}</span></li>
                                    <li>Date: <span>{new Date(orderObj?.createdAt).toDateString()}</span></li>
                                    <li>Shippig Method: <span>Delhivery Service</span></li>
                                    {/* <li>Payment Method: <span></span></li> */}
                                </ul>
                            </div>
                        </div>
                        <div className="col-12 offset-md-2 col-md-6">
                            <div className="order-address">
                                <p className='order-subheading'>SHIPPING ADDRESS</p>
                                <p><span>{orderObj?.addressObj?.line1},{orderObj?.addressObj?.line2},{orderObj?.addressObj?.city},{orderObj?.addressObj?.state}-{orderObj?.addressObj?.pincode}</span></p>
                                {/* <p className='order-subheading'>BILLING ADDRESS</p>
                                <p><span>Sushil Kumar 209, Civil Lines, Ludhiana Ludhiana, Punjab 540001 India</span></p> */}
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="order_table">
                <div className="container">
                    <div className="row">
                        {orderObj?.onlineDeliveryObj?.map((el, index) => {
                            return (
                                <div className="col-12">
                                    <div className="main_right_details myorder">
                                        <div className="col-md-12"><span>Shop : {el?.storeObj?.name}</span></div>
                                        <div className="col-md-12"><span>Address : {el?.storeObj?.address}</span></div>
                                        <div className="col-md-12"><span>Status : <b>{el?.statusObj?.status}</b></span></div>
                                        <div className="col-md-12"><span>Invoice Number : {el?.invoiceNumber}</span></div>
                                        {el?.delhiveryObj && el?.delhiveryObj?.packages && el?.delhiveryObj?.packages.length &&
                                            <div className="col-md-12"><span>Tracking Id : {el?.delhiveryObj?.packages[0]?.waybill} (You use this tracking id to track your package on Delhivery Services)</span></div>
                                        }





                                        <div className="card mt-2">
                                            <div className="card-header">
                                                <div className="row">
                                                    <div className="col-md-3"><span>PRODUCT NAME</span></div>
                                                    <div className="col-md-3"><span>UNIT PRICE</span></div>
                                                    <div className="col-md-3"><span>QUANTITY</span></div>
                                                    <div className="col-md-3"><span>LINE TOTAL</span></div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                {
                                                    el.productArr.map((ele, indexX) => {
                                                        return (

                                                            <div className="prCard row mb-2">
                                                                <div className="col-md-3">
                                                                    <h6>{ele?.productName}</h6>
                                                                    <span className="small">{ele?.variantDisplayArr.reduce((acc, elz) => acc + elz.outerVariantName + ":" + elz.innerVariantName + ",", '')}</span>
                                                                </div>
                                                                <div className="col-md-3">
                                                                    <p className="order-price">₹{(ele.realizedValue)}</p>
                                                                </div>
                                                                <div className="col-md-3">
                                                                    <h6>{ele?.quantity}</h6>
                                                                </div>
                                                                <div className="col-md-3">
                                                                    <p className="order-price">₹{(ele.realizedValue * ele?.quantity)}</p>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}

                    </div>
                </div>
            </section>

            <section id="order_total">
                <div className="container">
                    <div className="order_total_inner">
                        <div className="row">
                            <div className="col-12 col-md-4 offset-md-8">
                                <ul>
                                    <li>Subtotal<span>₹{(orderObj?.subTotalAmount)}</span></li>
                                    <li>Discount<span>- ₹{orderObj?.discountedAmountByCoupon}</span></li>
                                    {/* <li>Flat Rate<span>₹70.</span></li> */}
                                    <li>Total<span className='order-price'>₹{orderObj?.payableAmount}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    );
}

export default ViewOrder;