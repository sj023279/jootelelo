import React, { useEffect,useState } from 'react';
import backgroundImage1 from '../images/01/JPG/StoreImage.jpg'
import backgroundImage2 from '../images/02/JPG/StoreImage2.jpg'

import backgroundImage3 from '../images/03/JPG/StoreImage3.jpg'

import backgroundImage4 from '../images/04/JPG/StoreImage4.jpg'

import backgroundImage5 from '../images/05/JPG/StoreImage5.jpg'

import backgroundImage6 from '../images/06/JPG/StoreImage6.jpg'

import backgroundImage7 from '../images/07/JPG/StoreImage7.jpg'

function PageBanner({ name }) {
    let arr = [
        // backgroundImage1,
        // backgroundImage2,
        // backgroundImage3,
        // backgroundImage4,
        // backgroundImage5,
        backgroundImage6,
        // backgroundImage7,
    ]
    const [randomIntArr, setRandomIntArr] = useState(0);
    function getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }

    const handleRandomImageSelection = () => {
        let val=getRandomInt(7)
        setRandomIntArr(val)
    }
    useEffect(() => {
        handleRandomImageSelection()
    }, [])

    return (
        <section id="banner" style={{height:'400px'}}>
            <div className="product_banner" style={{ backgroundImage: `url(${arr[0]})`,height:400 }}>
                <div className="container">
                    {/* <h1 className="animate__animated animate__bounceInDown text-capitalize">{name}</h1> */}
                    <div className="banner_button animate__animated animate__bounceInDown">
                    </div>
                </div>
            </div>
        </section>
    );
}

export default PageBanner;