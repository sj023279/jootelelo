import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import $ from "jquery";
import LogoWhite from "../images/logos/logoWhite.png";

function Footer() {
  const [width, setWidth] = useState(window.innerWidth);
  const [height, setHeight] = useState(window.innerHeight);
  const updateDimensions = () => {
    console.log(window.innerWidth);
    setWidth(window.innerWidth);
    setHeight(window.innerHeight);
  };
  useEffect(() => {
    window.addEventListener("resize", updateDimensions);
    return () => window.removeEventListener("resize", updateDimensions);
  }, []);
  const cart_overlay = () => {
    $("#cart_section").removeClass("right_zero");
    $(".cart_overlay").css("display", "none");
    $("body").removeClass("overflow-hidden");
  };

  return (
    <>
      <section id="cart_section">
        <div className="cart_sectionmain">
          <div className="cartsec_head">
            <h5>Your Cart is Empty</h5>
            <span className="cross" onClick={() => cart_overlay()}>
              &times;
            </span>
          </div>
          <div className="cart-img">
            <img
              src="assets/images/empty-cart.png"
              className="img-fluid"
              alt=""
            />
            <Link
              to="/cart"
              onClick={() => cart_overlay()}
              className="view_cart"
            >
              view cart
            </Link>
          </div>
        </div>
      </section>
      <div className="cart_overlay" onClick={(e) => cart_overlay(e)}></div>
      <footer>
        <section id="footer">
          <div style={{}} className="container">
            <div className="col-sm-12 footer-logo_sec">
              <Link className="navbar-brand" to="/">
                <img
                  src={LogoWhite}
                  style={{ height: 100, width: 120 }}
                  alt=""
                />
              </Link>
            </div>
            <div className="row my-4">
              <div className=" col-lg-2 col-md-5 col-12 aboutstore_footer">
                {/* <h5>About Jootelelo</h5>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Possimus repudiandae perferendis nisi reiciendis, quae
                  nesciunt alias nam. Sint, quis ipsum.
                </p> */}
                <h5>Contact Us</h5>
                <div className="location_div socialmedea_sec">
                  <i className="fas fa-map-marker-alt"></i>
                  <p>
                    RZ-3A Sitapuri, New Delhi-110045. Near Dabri Metro Station
                  </p>
                </div>
                <div className="phone_div socialmedea_sec">
                  <i className="fas fa-phone"></i>
                  <p>
                    <Link to="tel:+91 9876543210">+91 9873439378</Link>
                  </p>
                </div>
                <div className="email_div socialmedea_sec">
                  <i className="far fa-envelope"></i>
                  <p>
                    <Link to="mailto:support@jootelelo.in">
                      support@jootelelo.in
                    </Link>
                  </p>
                </div>
              </div>
              {/* <div className="col-sm-2 offset-1 footer_links">
                <h5>My account</h5>
                <ul>
                  <li>
                    <Link to="#">Checkout</Link>
                  </li>
                  <li>
                    <Link to="/account">My Account</Link>
                  </li>
                  <li>
                    <Link to="#">My Orders</Link>
                  </li>
                  <li>
                    <Link to="#">Wishlist</Link>
                  </li>
                  <li>
                    <Link to="#">Login</Link>
                  </li>
                </ul>
              </div> */}
              {/* <div className="col-sm-2 footer_links">
                <h5>My account</h5>
                <ul>
                  <li><Link to='#'>Special</Link></li>
                  <li><Link to='#'>New Products</Link></li>
                  <li><Link to='#'>My Orders</Link></li>
                  <li><Link to='#'>Best Sellers</Link></li>
                  <li><Link to='#'>Our Stores</Link></li>
                </ul>
              </div> */}
              {width < 700 && <hr style={{ color: "white" }} />}
              <div className=" col-lg-3 offset-lg-1 col-md-3 col-12 footer_links">
                <h5>Our Collections</h5>
                <ul style={{ listStyleType: "none" }}>
                  <li style={{ marginTop: 10 }}>
                    <Link to="/about">Men</Link>
                  </li>
                  <li style={{ marginTop: 10 }}>
                    <Link to="/privacy">Women</Link>
                  </li>
                  {/* <li style={{ marginTop: 10 }}>
                    <Link to="#">Terms & Conditions</Link>
                  </li> */}
                  {/* <li style={{ marginTop: 10 }}>
                    <Link to="#">Our Stores</Link>
                  </li> */}
                </ul>
              </div>

              {width < 700 && <hr style={{ color: "white" }} />}

              <div className=" col-lg-3 col-md-3 col-12 footer_links">
                <h5>Information</h5>
                <ul style={{ listStyleType: "none" }}>
                  <li style={{ marginTop: 10 }}>
                    <Link to="/about">About Us</Link>
                  </li>
                  <li style={{ marginTop: 10 }}>
                    <Link to="/privacy">Privacy Policy</Link>
                  </li>
                  {/* <li style={{ marginTop: 10 }}>
                    <Link to="#">Terms & Conditions</Link>
                  </li> */}
                  <li style={{ marginTop: 10 }}>
                    <Link to="/faq">Help & FAQs</Link>
                  </li>
                  {/* <li style={{ marginTop: 10 }}>
                    <Link to="#">Our Stores</Link>
                  </li> */}
                </ul>
              </div>
              {width < 700 && <hr style={{ color: "white" }} />}

              <div className=" col-lg-3 col-md-4 col-12 contact_footer">
                {/* <h5 className="mb-3">Sign Up To Newsletter</h5>
                                <div className='d-flex align-items-center'>
                                    <input type="text" placeholder="Enter Your e-mail" className="footer_input" />
                                    <input type="submit" className="submit_footer" />
                                </div> */}
                <div className="followus_footer">
                  <h5 className="mt-3">Follow Us</h5>
                  <div className="footerextra">
                    <i className="fab fa-facebook-f"></i>
                    <i className="fab fa-twitter"></i>
                    <i className="fab fa-instagram"></i>
                    <i className="fab fa-youtube"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="copy_right">
            <p>
              Copyright &copy; 2021,{" "}
              <Link to="/" className="text-white">
                Jootelelo
              </Link>{" "}
              Made with <i className="fas fa-heart text-white"></i> by{" "}
              <Link to="ebslon.com" className="text-white">
                Ebslon Infotech
              </Link>
            </p>
          </div>
        </section>
      </footer>
    </>
  );
}

export default Footer;
