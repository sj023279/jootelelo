import React from "react";

function AboutSec1() {
  return (
    <section id="about_firstsec">
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-lg-12">
            <h3 className="text-center">Inside Jootelelo.in</h3>
            <p>
              When you heard of jootelelo you all would be finding it funny. But
              once you heard of it you won’t forget it. Not because of our name
              rather the products we offer you. jootelelo is a platform on which
              you will find large variety of shoes under one roof
              “jootelelo.in”.
            </p>
            <p>
              We are an online sports goods marketplace with experience centers
              in Delhi NCR. We specilize in a wide variety of footwear suitable
              for all customer segments. Our aim is to provide international
              sports trends to our customers with good quality and at an
              affordable price.
            </p>
            {/* <p>
              Jootelelo provides platform to small manufactures who makes good quality shoes but people are not aware of their brand . 
            </p>
            <p>
              We aim at providing you the best product at its best price. We aim that we have a trust with our customers. We value  your money spent on jootelelo that’s why we provide you the best products.
            </p> */}
            {/* <h3>Best of all sports shoes</h3>
                        <div className="about_points">
                            <div className="about_points_main">
                                <div className="row">
                                    <div className="col-sm-3">
                                        <img src="assets/images/about_icon_three-removebg-preview.png" className="img-fluid" alt='' />
                                    </div>
                                    <div className="col-sm-9">
                                        <h4>Rebuild your wardrobe</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor  </p>
                                    </div>
                                </div>
                            </div>
                            <div className="about_points_main">
                                <div className="row">
                                    <div className="col-sm-3">
                                        <img src="assets/images/about_icon_two-removebg-preview.png" className="img-fluid" alt='' />
                                    </div>
                                    <div className="col-sm-9">
                                        <h4>Soar into the sky</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor  </p>
                                    </div>
                                </div>
                            </div>
                            <div className="about_points_main">
                                <div className="row">
                                    <div className="col-sm-3">
                                        <img src="assets/images/about_icon_one-removebg-preview.png" className="img-fluid" alt='' />
                                    </div>
                                    <div className="col-sm-9">
                                        <h4>Take time off to cool off</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor </p>
                                    </div>
                                </div>
                            </div>
                        </div> */}
            {/* <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua
            </p> */}
          </div>
          {/* <div className="col-sm-4">
            <img
              src="assets/images/about_right.webp"
              className="img-fluid"
              alt=""
            />
          </div> */}
        </div>
      </div>
    </section>
  );
}

export default AboutSec1;
