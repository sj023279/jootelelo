import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

function Clients() {
  const responsive = {
    0: {
      items: 1,
    },
    576: {
      items: 1,
    },
    768: {
      items: 2,
    },
  };
  return (
    <section id="client_slider">
      <div className="container">
        <h3 className="heading">We have ties with</h3>
        <div className="client_slider_main">
          <OwlCarousel
            className="client owl-theme"
            items={responsive.items}
            smartSpeed="1000"
            autoplay
            loop
            margin={10}
            dots={false}
          >
            <div className="item">
              <div className="client_image_div mx-auto">
                <img
                  src="assets/images/client_logo_one.webp"
                  className="img-fluid"
                  alt=""
                />
              </div>
            </div>
            <div className="item">
              <div className="client_image_div mx-auto">
                <img
                  src="assets/images/client_logo_two.webp"
                  className="img-fluid"
                  alt=""
                />
              </div>
            </div>
            <div className="item">
              <div className="client_image_div mx-auto">
                <img
                  src="assets/images/client_logo_three.webp"
                  className="img-fluid"
                  alt=""
                />
              </div>
            </div>
            <div className="item">
              <div className="client_image_div mx-auto">
                <img
                  src="assets/images/client_logo_four.webp"
                  className="img-fluid"
                  alt=""
                />
              </div>
            </div>
          </OwlCarousel>
        </div>
      </div>
    </section>
  );
}

export default Clients;
