import React, { useState, useContext, createContext } from 'react';
import ProductFilter from './products/product-filter';
import ProductBanner from './products/products-banner';


export const productFilterContext = createContext()
function Products() {
    const [filterId, setFilterId] = useState('');
    return (
        <productFilterContext.Provider value={[filterId, setFilterId]}>
            <ProductBanner />
            <ProductFilter />
        </productFilterContext.Provider>
        // </>
    );
}



export default Products;