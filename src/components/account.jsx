import React, { useState, useEffect, useContext } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import $ from "jquery";
import { authContext } from "../App";
function Account({ active_account }) {
  const [isAuthorized, setIsAuthorized] = useContext(authContext);
  const navigate = useNavigate();
  const [activeTab, setActiveTab] = useState(0);
  const location = useLocation();
  let tab_active = (e) => {
    console.log(active_account, e.currentTarget);
    // if (active_account) {
    $(".myclass_tab").removeClass("tab-active");
    $(e.currentTarget).addClass("tab-active");
    // }
  };

  const handleLogout = () => {
    localStorage.removeItem("TOKEN");
    setIsAuthorized(false);
    navigate("/");
  };

  useEffect(() => {
    console.log(location);
    if (location.pathname == "/account") {
      setActiveTab(0);
    } else if (location.pathname == "/account/order") {
      setActiveTab(1);
    } else if (location.pathname == "/account/wishlist") {
      setActiveTab(2);
    } else if (location.pathname == "/account/address") {
      setActiveTab(3);
    }
  }, [location]);

  return (
    <div className="left_account_info position-relative">
      <div className="user-account">
        <i className="far fa-user accountuser"></i>
      </div>
      <ul>
        <li>
          <Link
            to="/account"
            className={`myclass_tab ${activeTab == 0 ? "tab-active" : ""}`}
          >
            <i className="far fa-user"></i>
            <p> My Account</p>
          </Link>
        </li>
        <li>
          <Link
            to="/account/order"
            className={`myclass_tab ${activeTab == 1 ? "tab-active" : ""}`}
          >
            <i className="fas fa-cart-arrow-down"></i>
            <p> My Order</p>
          </Link>
        </li>
        <li>
          <Link
            to="/account/wishlist"
            className={`myclass_tab ${activeTab == 2 ? "tab-active" : ""}`}
          >
            <i className="far fa-heart"></i>
            <p> My Wishlist</p>
          </Link>
        </li>
        {/* <li>
                    <Link to='/account/review' className="myclass_tab" onClick={(e) => tab_active(e)}><i className="far fa-comment-dots"></i><p> My Reviews</p></Link>
                </li> */}
        <li>
          <Link
            to="/account/address"
            className={`myclass_tab ${activeTab == 3 ? "tab-active" : ""}`}
          >
            <i className="far fa-address-card"></i>
            <p> My Addresses </p>
          </Link>
        </li>
        <li>
          <span className="myclass_tab" onClick={() => handleLogout()}>
            <i className="fas fa-sign-out-alt"></i>
            <p> log Out</p>
          </span>
        </li>
      </ul>
    </div>
  );
}
export default Account;
