import React, { useState, useEffect, useContext } from "react";
import { getAllCategory } from "../../services/Category";
import { generateImageUrl } from "../../services/url";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/autoplay";
import { productFilterContext } from "../products";

function ProductBanner() {
  const [categoryArr, setCategoryArr] = useState([]);
  const [currentFilter, setcurrentFilter] = useState("");
  const [responsive, setResponsive] = useState({
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    960: {
      items: 4,
    },
  });

  const [filterId, setFilterId] = useContext(productFilterContext);

  const getCategories = async () => {
    try {
      const { data: res } = await getAllCategory();
      if (res.success) {
        setCategoryArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getCategories();
  }, []);

  useEffect(() => {
    handleFilterCategory();
  }, [filterId]);

  const handleFilterCategory = () => {
    let categoryObj = categoryArr.find((el) => el._id == filterId);
    if (categoryObj?.name) {
      setcurrentFilter(categoryObj?.name);
    }
  };

  return (
    <section id="products_banner">
      <div className="container filter_slider_container">
        <div className="col-sm-6 m-auto text-center">
          <h3 className="heading" style={{ textTransform: "capitalize" }}>
            {currentFilter.toLowerCase()} Collection
          </h3>
          {/* {
                        categoryArr.length > 0 &&
                        <div className="products_banner_main">
                            <Swiper
                                spaceBetween={50}
                                modules={[Autoplay]}
                                slidesPerView={3}
                                autoplay={true}
                                zoom={true}
                                // onSlideChange={() => console.log('slide change')}
                                // onSwiper={(swiper) => console.log(swiper)}
                            >
                                {categoryArr.map((item, index) => {
                                    return (
                                        <SwiperSlide key={index}>
                                            <div key={index} className="item" onClick={() => setFilterId(item._id)}>
                                                <div className="products_filter_slider">
                                                    <div className="extra">
                                                        <img src={generateImageUrl(item?.thumbnailImage?.url)} className="img-fluid" alt='' />
                                                        <span>{item.name}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </SwiperSlide>
                                    )
                                })}

                            </Swiper>

                        </div>
                    } */}
        </div>
      </div>
    </section>
  );
}

export default ProductBanner;
