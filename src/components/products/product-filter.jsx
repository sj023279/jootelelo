import React, { useEffect, useState, useContext } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import { loadingContext } from "../../App";
import { getAllCategory } from "../../services/Category";
import {
  getAllGenders,
  getAllProducts,
  getAllDiscount,
  getProductForUsersWebHomepage,
} from "../../services/Product";
import { addToWishlist } from "../../services/wishlist";
import { productFilterContext } from "../products";
import Slider, { Range } from "rc-slider";
import "rc-slider/assets/index.css";
import ProductImage from "../../images/bestseller_shoe_newfive.webp";

import { getVariantForFilter } from "../../services/Variant";

function ProductFilter() {
  const [priceRange, setPriceRange] = useState([10, 5000]);
  const [productArr, setProductArr] = useState([]);
  const [mainProductArr, setMainProductArr] = useState([]);

  const [searchParams, setSearchParams] = useSearchParams();

  const [categoryArr, setCategoryArr] = useState([]);
  const [isLoading, setIsLoading] = useContext(loadingContext);
  const [filterId, setFilterId] = useContext(productFilterContext);
  // console.log('productFilterContext', productFilterContext)

  const [nestedFilterArr, setNestedFilterArr] = useState([]);
  const [variantArr, setVariantArr] = useState([]);
  const navigate = useNavigate();

  const [readMoreState, setReadMoreState] = useState(false);
  const [showFilters, setShowFilters] = useState(true);
  const [filterBtn, setFilterBtn] = useState(true);
  const [genderArr, setGenderArr] = useState([]);
  const [discountArr, setDiscountArr] = useState([]);

  const [filterObj, setFilterObj] = useState({
    categoryId: "",
    sortBy: "Latest",
    gender: "ALL",
    // discount:'Fresh Stock',
    priceRange: {
      maxPrice: 5000,
      minPrice: 0,
    },
    variantArr: [],
  });
  const [selectedCategoryId, setSelectedCategoryId] = useState("");
  const [SortFilter, setSortFilter] = useState("");
  const getAllProductsOnInit = async () => {
    setIsLoading(true);
    try {
      let { data: res } = await getProductForUsersWebHomepage(1, 100);
      // console.log(res);
      if (res.data) {
        let tempArr = res.data
          .filter((el) => el.imageArr && el.imageArr.length > 0)
          .map((el) => {
            let obj = {
              ...el,
              img1:
                el.imageArr?.length && el.imageArr[0]?.url
                  ? el.imageArr[0]?.url
                  : ProductImage,
              img2:
                el.imageArr?.length && el.imageArr[0]?.url
                  ? el.imageArr[0]?.url
                  : ProductImage,
            };
            return obj;
          });

        let searchParamsData = {
          gender: searchParams.get("gender"),
          category: searchParams.get("category"),
          searchRes: searchParams.get("search"),
        };
        setMainProductArr([...tempArr]);
        if (searchParamsData.searchRes) {
          console.log("Load Search Data");
          tempArr = tempArr.filter((el) =>
            el.name
              .toLowerCase()
              .includes(searchParamsData.searchRes.toLowerCase())
          );
          setProductArr([...tempArr]);
        }
        if (searchParamsData.gender) {
          console.log(searchParamsData.category);
          if (searchParamsData.category) {
            console.log("CATEGOGY EMPTY");
            setFilterId(searchParamsData.category);
            let tempArr1 = tempArr.filter(
              (el) =>
                el.gender.toLowerCase() ==
                  searchParamsData.gender.toLowerCase() &&
                el.categoryId == searchParamsData.category
            );
            setProductArr([...tempArr1]);
          } else {
            let tempArr1 = tempArr.filter(
              (el) =>
                el.gender.toLowerCase() == searchParamsData.gender.toLowerCase()
            );
            setProductArr([...tempArr1]);
          }
        } else {
          setProductArr([...tempArr]);
        }
      }
    } catch (err) {
      console.log(err);
    }
    setIsLoading(false);
  };

  const getAllVariants = async () => {
    try {
      let { data: res } = await getVariantForFilter();
      if (res.data) {
        let tempArr = [...res.data];
        tempArr = tempArr
          .map((el) => {
            let obj = {
              ...el,
              displayReadMore: false,
              variantArr: el.variantArr.map((el, i) => {
                let obj1 = {
                  ...el,
                  checked: false,
                };
                return obj1;
              }),
            };
            return obj;
          })
          .filter((el) => el.name != "Type");

        setVariantArr([...tempArr]);
      }
    } catch (err) {
      console.log(err);
    }
    // setIsLoading(false)
  };

  const getCategories = async () => {
    try {
      const { data: res } = await getAllCategory();
      if (res.success) {
        setCategoryArr(res.data);
        let searchParamsData = searchParams.get("category");
        setSelectedCategoryId(searchParamsData);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSinglePageRedirect = (id) => {
    navigate(`/single-product/${id}`);
  };

  const addItemToWishlist = async (item) => {
    try {
      let obj = {
        productId: item.productId,
        variantId: item.variantId,
      };
      const { data: res } = await addToWishlist(obj);
      if (res.success) {
        alert(res.message);
      }
    } catch (error) {
      console.error(error);
    }
  };

  function filterAll(filterObj) {
    // filter by category
    let tempArr = [...mainProductArr];
    console.log("{filter}", tempArr);
    let tempNullArr = [...mainProductArr.filter((el) => el.categoryId == null)];

    if (filterObj.categoryId) {
      tempArr = tempArr.filter((el) => el.categoryId == filterObj.categoryId);

      tempArr = [...tempArr, ...tempNullArr];
      // setProductArr();
      setSelectedCategoryId(filterObj.categoryId);
    }
    console.log("{filter} : category", filterObj, tempArr);
    // console.log("{filter} : category")

    // // gender
    if (filterObj.gender.toLowerCase() == "all") {
      // tempArr = [...tempArr];
    } else {
      tempArr = tempArr.filter(
        (el) => el.gender.toLowerCase() == filterObj.gender.toLowerCase()
      );
      tempArr = [...tempArr];
      // setNestedFilterArr([...tempArr])
    }

    console.log("{filter} : gender", filterObj, tempArr);

    //discount
    if (filterObj.discount) {
      // console.log(filterObj,tempArr)
      if (filterObj.discount == "Fresh Stock") {
        tempArr = tempArr.filter((el) => el.discount == 0);
        // setProductArr([...tempArr]);
      } else {
        tempArr = tempArr.filter((el) => el.discount == filterObj.discount);
        // setProductArr([...tempArr]);
        // setNestedFilterArr([...tempArr])
      }
    }
    console.log("{filter} : discount", filterObj, tempArr);

    // handlePriceFilter

    tempArr = tempArr.filter(
      (el) =>
        el.realizedValue >= filterObj.priceRange.minPrice &&
        el.realizedValue <= filterObj.priceRange.maxPrice
    );

    console.log("{filter} : price", filterObj, tempArr);

    // variants

    if (filterObj.variantArr.length) {
      let anySelected = filterObj.variantArr.some((el) =>
        el.variantArr.some((elx) => elx.checked)
      );

      if (anySelected) {
        let selectedVariantArr = filterObj.variantArr.filter((el) =>
          el.variantArr.some((elx) => elx.checked)
        );
        console.log("selectedVariantArr", selectedVariantArr);
        tempArr = tempArr.filter((el) => {
          let productVariantArr = el.selectedVariants.filter((el) =>
            selectedVariantArr.some((elx) => elx.variantId == el.variantId)
          );

          if (selectedVariantArr?.length && productVariantArr?.length) {
            let val = true;
            selectedVariantArr.forEach((elx) => {
              let productVariantObj = productVariantArr.find(
                (elz) => elz.variantId == elx.variantId
              );
              if (
                !productVariantObj.variantArr.some((elz) =>
                  elx.variantArr
                    .filter((x) => x.checked)
                    .some((x) => x.variantId == elz.variantId)
                )
              ) {
                val = false;
              }
            });
            console.log(val, el.name);
            return val;
          }

          return true;
        });
      }
    }

    // sort
    if (filterObj.sortBy == "Latest") {
      // sort by latest
    } else if (filterObj.sortBy == "TopRated") {
      tempArr.sort((a, b) => b.rating - a.rating);
    } else if (filterObj.sortBy == "LowToHigh") {
      tempArr.sort((a, b) => a.realizedValue - b.realizedValue);
    } else if (filterObj.sortBy == "HighToLow") {
      tempArr.sort((a, b) => b.realizedValue - a.realizedValue);
    }

    console.log("{filter} : sort", filterObj, tempArr);

    setProductArr([...tempArr]);
  }

  const handleFilterByCategory = async (filterId) => {
    // let tempArr = [...mainProductArr];
    // let tempNullArr = [...mainProductArr.filter((el) => el.categoryId == null)];
    // if (filterId) {
    //   tempArr = tempArr.filter((el) => el.categoryId == filterId);
    //   setProductArr([...tempArr, ...tempNullArr]);
    //   setSelectedCategoryId(filterId)
    // }
    setFilterId(filterId);
    let obj = {
      ...filterObj,
      categoryId: filterId,
    };

    setFilterObj((prevState) => obj);
    filterAll(obj);
  };

  const handleSortBy = (value) => {
    setSortFilter(value);
    // if (value == "Latest") {
    //   setProductArr([...productArr]);
    // } else if (value == "TopRated") {

    //   let tempArr = [...productArr];
    //   tempArr.sort((a, b) => b.rating - a.rating);
    //   setProductArr([...tempArr]);
    // } else if (value == "LowToHigh") {

    //   let tempArr = [...productArr];
    //   tempArr.sort((a, b) => a.realizedValue - b.realizedValue);
    //   setProductArr([...tempArr]);

    // } else if (value == "HighToLow") {

    //   let tempArr = [...productArr];
    //   tempArr.sort((a, b) => b.realizedValue - a.realizedValue);
    //   setProductArr([...tempArr]);

    // }
    let obj = {
      ...filterObj,
      sortBy: value,
    };
    setFilterObj((prevState) => obj);
    filterAll(obj);
  };

  const handleFilterByGender = (value) => {
    // if (value == "All") {
    //   setProductArr([...productArr]);
    // } else if (value == "MALE") {
    //   let tempArr = [...productArr];
    //   tempArr = tempArr.filter((el) => el.gender == "MALE");
    //   setProductArr([...tempArr]);
    //   setNestedFilterArr([...tempArr])

    // } else {
    //   let tempArr = [...productArr];
    //   tempArr = tempArr.filter((el) => el.gender != "MALE");
    //   setProductArr([...tempArr]);
    //   setNestedFilterArr([...tempArr])

    // }
    let obj = {
      ...filterObj,
      gender: value,
    };

    setFilterObj((prevState) => obj);
    filterAll(obj);
  };

  const handleFilterByDiscount = (value) => {
    let obj = {
      ...filterObj,
      discount: value,
    };

    setFilterObj((prevState) => obj);
    filterAll(obj);
  };

  const handlePriceFilter = () => {
    // let tempProductArr = [...productArr];

    // tempProductArr = tempProductArr.filter(
    //   (el) =>
    //     el.realizedValue >= priceRange[0] && el.realizedValue <= priceRange[1]
    // );
    // setProductArr(tempProductArr);
    let obj = {
      ...filterObj,
      priceRange: { maxPrice: priceRange[1], minPrice: priceRange[0] },
    };
    setFilterObj((prevState) => obj);

    filterAll(obj);
  };

  const handleVariantArrSelection = (index) => {
    let tempArr = [...variantArr];
    tempArr[index].displayReadMore = !tempArr[index].displayReadMore;
    setVariantArr([...tempArr]);
  };

  const handleVariantFilter = (mainVariantId, innerVariantId) => {
    // let tempProductArr = [...mainProductArr];
    // tempProductArr = tempProductArr.filter((elz) =>
    //   elz.selectedVariants.some(
    //     (el) =>
    //       el.variantId == mainVariantId &&
    //       el.variantArr.some((elx) => elx.variantId == innerVariantId)
    //   )
    // );
    // setProductArr([...tempProductArr]);

    let tempArr = [...variantArr];
    let outerIndex = variantArr.findIndex(
      (el) => el.variantId == mainVariantId
    );
    if (outerIndex != -1) {
      let innerIndex = tempArr[outerIndex].variantArr.findIndex(
        (elx) => elx.variantId == innerVariantId
      );
      if (innerIndex != -1) {
        tempArr[outerIndex].variantArr[innerIndex].checked =
          !tempArr[outerIndex].variantArr[innerIndex].checked;
      }
    }

    let obj = { ...filterObj, variantArr: tempArr };
    setFilterObj((prevState) => obj);

    setVariantArr([...tempArr]);
    filterAll(obj);
  };

  useEffect(() => {
    handleFilterByCategory();
    let w = window.screen.width;
    if (w > 900) {
      setFilterBtn(false);
    }
  }, [filterBtn]);

  const getGender = async () => {
    try {
      const { data: res } = await getAllGenders();
      if (res.success) {
        setGenderArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };
  const getDiscounts = async () => {
    try {
      const { data: res } = await getAllDiscount();
      if (res.success) {
        setDiscountArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getAllProductsOnInit();
    getGender();
    getDiscounts();
    getCategories();
    getAllVariants();
  }, [
    searchParams.get("gender"),
    searchParams.get("category"),
    searchParams.get("search"),
  ]);

  return (
    <section id="main_filtersec">
      <div className="container">
        {filterBtn && (
          <button
            onClick={() => setShowFilters(!showFilters)}
            type="button"
            class="btn btn-warning mb-2"
          >
            Show Filters
          </button>
        )}
        <div className="row">
          {showFilters && (
            <div className="col-sm-3">
              <div
                className="left_filter_div"
                style={{
                  position: "sticky",
                  top: 100,
                  overflowY: "auto",
                  height: "calc( 100vh - 106px)",
                }}
              >
                <div className="category backgroun_filter_property">
                  <h5 className="filter_heading">Category</h5>
                  <ul className="filter_lilst">
                    {categoryArr.map((item, index) => {
                      return (
                        <li
                          key={index}
                          onClick={() => handleFilterByCategory(item._id)}
                          className="main_list d-flex"
                        >
                          <input
                            type="radio"
                            className="my-auto"
                            name="category"
                            id={`category${index}`}
                            // value={ely.checked}
                            checked={selectedCategoryId == item._id}
                            style={{ marginRight: 10 }}
                          />
                          <label
                            for={`category${index}`}
                            className="size_filter_box"
                          >
                            {item.name}
                          </label>
                        </li>
                      );
                    })}
                  </ul>
                </div>
                <div className="Color_filter backgroun_filter_property">
                  <h5 className="filter_heading">Sort By</h5>
                  <ul className="color_filter_list">
                    <li
                      className="size_filter_box d-flex"
                      onClick={() => handleSortBy("Latest")}
                    >
                      <input
                        type="radio"
                        className="my-auto"
                        name="sortby"
                        id={1}
                        // value={ely.checked}
                        checked={SortFilter == "Latest"}
                        style={{ marginRight: 10 }}
                      />
                      <label for={1}>Latest</label>
                    </li>

                    <li
                      className="size_filter_box d-flex"
                      onClick={() => handleSortBy("LowToHigh")}
                    >
                      <input
                        type="radio"
                        className="my-auto"
                        name="sortby"
                        id={2}
                        // value={ely.checked}
                        checked={SortFilter == "LowToHigh"}
                        style={{ marginRight: 10 }}
                      />
                      <label for={2}>Price: Low To High</label>
                    </li>

                    <li
                      className="size_filter_box d-flex"
                      onClick={() => handleSortBy("HighToLow")}
                    >
                      <input
                        type="radio"
                        className="my-auto"
                        name="sortby"
                        id={3}
                        // value={ely.checked}
                        // checked={el.checked}
                        checked={SortFilter == "HighToLow"}
                        style={{ marginRight: 10 }}
                      />
                      <label for={3}> Price: High To Low</label>
                    </li>
                  </ul>
                </div>
                <div className="Color_filter backgroun_filter_property">
                  <h5 className="filter_heading">Sort By Gender</h5>
                  <ul className="color_filter_list">
                    <li
                      className="size_filter_box"
                      onClick={() => handleFilterByGender("All")}
                    >
                      <input
                        type="radio"
                        className="my-auto"
                        name="gender"
                        id="gender"
                        // value={ely.checked}
                        // checked={el.checked}
                        style={{ marginRight: 10 }}
                      />
                      <label for="gender">All</label>
                    </li>
                    {genderArr.map((el, index) => {
                      return (
                        <li
                          className="size_filter_box"
                          onClick={() => handleFilterByGender(el)}
                        >
                          <input
                            type="radio"
                            className="my-auto"
                            name="gender"
                            id={`gender${index}`}
                            // value={ely.checked}
                            // checked={el.checked}
                            style={{ marginRight: 10 }}
                          />
                          <label for={`gender${index}`}>{el}</label>
                        </li>
                      );
                    })}
                  </ul>
                </div>
                <div className="Color_filter backgroun_filter_property">
                  <h5 className="filter_heading">Discounts Available</h5>
                  <ul className="color_filter_list">
                    <li
                      className="size_filter_box"
                      onClick={() => handleFilterByDiscount("Fresh Stock")}
                    >
                      <input
                        type="radio"
                        className="my-auto"
                        name="discount"
                        id="fresh"
                        // value={ely.checked}
                        // checked={el.checked}
                        style={{ marginRight: 10 }}
                      />
                      <label for="fresh">Fresh Stock</label>
                    </li>
                    {discountArr.map((el, index) => {
                      return (
                        <li
                          className="size_filter_box"
                          style={{
                            display: "flex",
                            alignItems: "center",
                            flexDirection: "row",
                          }}
                          onClick={() => handleFilterByDiscount(el)}
                        >
                          <input
                            type="radio"
                            className="my-auto"
                            name="discount"
                            id={`Off${index}`}
                            // value={ely.checked}
                            // checked={el.checked}
                            style={{ marginRight: 10 }}
                          />
                          <label for={`Off${index}`}>
                            {Math.round(el * 100)}% OFF
                          </label>
                        </li>
                      );
                    })}
                  </ul>
                </div>
                <div className="size_filter backgroun_filter_property">
                  {variantArr.map((el, index) => {
                    return (
                      <>
                        <h5 key={el._id} className="filter_heading">
                          {el.name}
                        </h5>
                        {el?.variantArr?.length && (
                          <div
                            // style={{ display: 'flex', flexDirection: 'row' , alignItems:"center"}}
                            className={"size_filter_main"}
                          >
                            {el.name == "Color" ? (
                              <>
                                {/* {el.variantArr.filter(el=>el.colorHex && el.colorHex!="")
                                  .filter(
                                    (elz, i) => el.displayReadMore || i < 6
                                  )
                                  .map((ely, innerIndex) => {
                                    return (
                                      <>
                                        <div
                                        style={{backgroundColor:ely.colorHex}}
                                          onClick={() =>
                                            handleVariantFilter(
                                              el.variantId,
                                              ely.variantId
                                            )
                                          }
                                          key={ely._id}
                                          className="filter_color"
                                        >
                                          <input
                                            type="checkbox"
                                            value={ely.checked}
                                            checked={el.checked}
                                            style={{ marginRight: 10 }}
                                          />
                                          <span>{ely.name}</span>
                                        </div>
                                      </>
                                    );
                                  })} */}
                                {el.variantArr
                                  .filter(
                                    (el) => el.colorHex && el.colorHex != ""
                                  )
                                  .filter(
                                    (elz, i) => el.displayReadMore || i < 6
                                  )
                                  .map((ely, innerIndex) => {
                                    return (
                                      <>
                                        <div
                                          style={{
                                            backgroundColor: ely.colorHex,
                                            borderWidth: ely.checked ? 4 : 1,
                                          }}
                                          onClick={() =>
                                            handleVariantFilter(
                                              el.variantId,
                                              ely.variantId
                                            )
                                          }
                                          key={ely._id}
                                          className="filter_color"
                                        ></div>
                                      </>
                                    );
                                  })}
                              </>
                            ) : (
                              <>
                                {el.variantArr
                                  .filter(
                                    (elz, i) => el.displayReadMore || i < 6
                                  )
                                  .map((ely) => {
                                    return (
                                      <>
                                        <div
                                          key={ely._id}
                                          onClick={() =>
                                            handleVariantFilter(
                                              el.variantId,
                                              ely.variantId
                                            )
                                          }
                                          style={{
                                            backgroundColor:
                                              !ely.checked && "#F5F5F5",
                                            width: 40,
                                          }}
                                          className="size_filter_box"
                                        >
                                          {ely.name}
                                        </div>
                                      </>
                                    );
                                  })}
                              </>
                            )}
                            {el.variantArr.length > 6 && (
                              <p
                                style={{
                                  color: "#516EAE",
                                  marginBottom: 10,
                                  display: "block",
                                  cursor: "pointer",
                                }}
                                onClick={() => handleVariantArrSelection(index)}
                              >
                                {el.displayReadMore ? "See Less" : "See More"}
                              </p>
                            )}
                          </div>
                        )}
                      </>
                    );
                  })}
                </div>
                <div className="price_filter backgroun_filter_property">
                  <h5 className="filter_heading">Price</h5>
                  <div className="range-slider">
                    <Range
                      max={5000}
                      min={10}
                      marks={true}
                      allowCross={false}
                      value={priceRange}
                      onChange={(val) => setPriceRange(val)}
                      onAfterChange={() => handlePriceFilter()}
                      railStyle={{ backgroundColor: "#fce869" }}
                    />
                    <div className="d-flex justify-content-between">
                      <span className="price_range_value">{priceRange[0]}</span>
                      <span className="price_range_value">{priceRange[1]}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
          <div className="col-sm-9 pe-0">
            <div className="right_filter_div row">
              {productArr.length > 0 ? (
                <>
                  {productArr.map((item, index) => {
                    return (
                      <div
                        key={index}
                        className="filter_right_container col-12 col-lg-3 col-md-4"
                        onClick={() => handleSinglePageRedirect(item.productId)}
                      >
                        <div className="best_seller_container">
                          <div className="seller_container_main">
                            <img src={item.img1} className="img-fluid" alt="" />
                            {/* <img src={item.img2} className="img-fluid productimage_two" alt='' /> */}
                            {item.discount <= 0.3 && (
                              <div className="new_arrival">
                                <p>{Math.round(item.discount * 100)}% Off</p>
                              </div>
                            )}
                            {/* {item.discount > 0 && (
                              <div className="discount">
                                <p>
                                  {Math.round(item.discount * 100)}% Discount
                                </p>
                              </div>
                            )} */}
                          </div>
                          <div className="product_about">
                            <h5 className="shoe_name">{item.article}</h5>

                            <h6>{item.brandName}</h6>
                            <h6>{item.name}</h6>

                            <div className="price_sec">
                              <div className="extra">
                                {item.discount > 0 ? (
                                  <div
                                    className="mb-2"
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                      width: 200,
                                      justifyContent: "space-between",
                                    }}
                                  >
                                    <span>
                                      <s>₹{Math.round(item.price)}</s>
                                    </span>
                                    {/* <span>Dis {item.discount * 100}% Off</span> */}
                                    <span>
                                      <b>₹{Math.round(item.realizedValue)}</b>
                                    </span>
                                  </div>
                                ) : (
                                  <h5>
                                    <b>₹{Math.round(item.realizedValue)}</b>
                                  </h5>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </>
              ) : (
                <span>No Products Found</span>
              )}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default ProductFilter;
