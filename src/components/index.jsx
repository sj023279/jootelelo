import React from "react";

import Slider from "./index/slider";
import Banners from "./index/banners";
import ProductSlider from "./index/product-slider";
import ShoppingFor from "./index/shopping-for";
import BestSeller from "./index/best-seller";
import DealBanner from "./index/deal-banner";
import OfflineStore from "./index/offline-store";
import Benifits from "./index/Benifits";
import BannerStatic from "./index/BannerStatic";

function Index() {
  return (
    <>
      <Slider position="TOP" />
      {/* <Banners /> */}

      <ProductSlider heading="Trending Collections" />
      <ShoppingFor />
      <Benifits />
      <ProductSlider heading="Men's Collection" />
      <BannerStatic />
      <ProductSlider heading="Women's Collection" />

      {/* <Slider position="MIDDLE" /> */}
      <ProductSlider heading="New Arrivals" />

      {/* <BestSeller /> */}
      <DealBanner />
      <OfflineStore />
    </>
  );
}

export default Index;
