import React from "react";

function ProductDetail({ variantObj }) {
  const openCity = (evt, cityName) => {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  };

  return (
    <section id="tab_discription">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="tab">
              <button
                className="tablinks active"
                onClick={(e) => openCity(e, "London")}
              >
                Discription
              </button>
              <button
                className="tablinks"
                onClick={(e) => openCity(e, "Paris")}
              >
                Additional information
              </button>
            </div>

            <div
              id="London"
              className="tabcontent"
              style={{ display: "block" }}
            >
              {/* <h4>Discription</h4> */}
              <p>{variantObj?.ProductDescription}</p>
            </div>

            <div id="Paris" className="tabcontent">
              <h4>Material & Care</h4>
              <p className="product_discription">Upper : {variantObj?.Upper}</p>
              <p className="product_discription">Sole : {variantObj?.Sole}</p>
              <p className="product_discription">
                Insole : {variantObj?.Insole}
              </p>
              <p className="product_discription">
                Care Instructions :{variantObj?.CareInstruction}
              </p>{" "}
            </div>

            <div className="clearfix"></div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default ProductDetail;
