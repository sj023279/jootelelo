import React, { useState, useEffect, useContext } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import {
  getAllProducts,
  getNewArrivals,
  getSimilarProducts,
} from "../../services/Product";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/autoplay";

import ProductImage from "../../images/bestseller_shoe_newfive.webp";
import { addToWishlist } from "../../services/wishlist";

import { authContext, headerContext, loadingContext } from "../../App";
import "swiper/css/navigation";
import { Navigation } from "swiper";
function SimilarProducts(props) {
  const [productArr, setProductArr] = useState([]);
  console.log(props);
  const navigate = useNavigate();
  const [isAuthorized, setIsAuthorized] = useContext(authContext);
  const [toggleHeader, setToggleHeader] = useContext(headerContext);
  const [isLoading, setIsLoading] = useContext(loadingContext);
  const prevRef = React.useRef();
  const nextRef = React.useRef();

  const responsive = {
    0: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    576: {
      slidesPerView: 1,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
  };

  const getAllProductsOnInit = async () => {
    // setIsLoading(true)
    try {
      let { data: res } = await getSimilarProducts(props.productId);
      if (res.data) {
        let tempArr = res.data
          .filter((el) => el.imageArr && el.imageArr.length > 0)
          .map((el) => {
            let obj = {
              ...el,
              img1:
                el.imageArr?.length && el.imageArr[0]?.url
                  ? el.imageArr[0]?.url
                  : ProductImage,
              img2:
                el.imageArr?.length && el.imageArr[0]?.url
                  ? el.imageArr[0]?.url
                  : ProductImage,
            };
            return obj;
          });
        console.log(tempArr, "similar ");
        setProductArr([...tempArr]);
      }
    } catch (err) {
      console.log(err);
    }
    // setIsLoading(false)
  };

  const handleSinglePageRedirect = (id) => {
    navigate(`/single-product/${id}`);
  };

  useEffect(() => {
    getAllProductsOnInit();

    return () => setProductArr([]);
  }, []);

  return (
    <section id="shop_now_slider">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="row">
              <div className="col-4">
                <h3 className="headingMain">Similar Products</h3>
              </div>
              <div className="col-6">
                {/* <div className="bottomLine"></div> */}
              </div>
              <div className="col-2  d-flex  align-items-center justify-content-center ">
                <i
                  class="fa-solid fa-circle-left"
                  ref={prevRef}
                  style={{ fontSize: 20, marginRight: 5, cursor: "pointer" }}
                ></i>
                <i
                  class="fa-solid fa-circle-right"
                  ref={nextRef}
                  style={{ fontSize: 20, cursor: "pointer" }}
                ></i>
              </div>
            </div>
          </div>
          {/* <div className="d-flex align-items-center justify-content-center">
            <div className="bottomLine"></div>
          </div> */}
        </div>
        {/* <h3 className="heading">Similar Products</h3> */}
        {productArr.length > 0 && (
          <Swiper
            spaceBetween={500}
            modules={[Autoplay, Navigation]}
            allowSlidePrev={true}
            breakpoints={responsive}
            autoplay={true}
            zoom={true}
            navigation={{
              prevEl: prevRef?.current,
              nextEl: nextRef?.current,
            }}
          >
            {productArr.map((item, index) => {
              return (
                <SwiperSlide key={index}>
                  <div
                    className="item"
                    onClick={() => handleSinglePageRedirect(item.productId)}
                    style={{ cursor: "pointer" }}
                  >
                    <div className="best_seller_container">
                      <div
                        className="seller_container_main"
                        style={{ maxHeight: 300, minHeight: 300 }}
                      >
                        <img src={item.img1} className="img-fluid " alt="" />
                        {item.discount > 0 && (
                          <div className="new_arrival">
                            <p>
                              {item.discount * 100}% <br />
                              Off
                            </p>
                          </div>
                        )}
                      </div>
                      <div className="product_about">
                        <h6>{item.brandName}</h6>
                        <h6>{item.name}</h6>

                        <div className="extra">
                          {item.discount > 0 ? (
                            <div
                              className="mb-2"
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                width: "137%",
                                justifyContent: "space-between",
                              }}
                            >
                              <div className="col-6">
                                <span>
                                  <s>₹ {Math.round(item.price)}</s>
                                </span>
                              </div>
                              <div className="col-6">
                                <span>₹ {Math.round(item.realizedValue)}</span>
                              </div>
                            </div>
                          ) : (
                            <div
                              className="mb-2"
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                width: "137%",
                                justifyContent: "space-between",
                              }}
                            >
                              <div className="col-6">
                                <span>₹{Math.round(item.price)}</span>
                              </div>
                            </div>
                          )}

                          {/* 
                                                    <span>Sizes : </span>
                                                    {item.variantList && item.variantList.size && item.variantList.size.map((ele, sizeIndex) => {
                                                        return (
                                                            <span>{ele.name} {sizeIndex != item.variantList.size.length - 1 && ","}</span>
                                                        )
                                                    })} */}
                        </div>
                      </div>
                    </div>
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>
        )}
      </div>
    </section>
  );
}

export default SimilarProducts;
