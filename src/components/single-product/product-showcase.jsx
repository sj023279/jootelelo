import React, { useEffect, useState, useContext } from "react";

import { Navigate, useNavigate, useParams } from "react-router-dom";

import { getProductById } from "../../services/Product";

import ProductImage from "../../images/bestseller_shoe_ninetwo.webp";

import checkedImage from "../../images/checked.png";

import PageBanner from "../page-banner";

import { addItemToCart } from "../../services/user";
import ImageGallery from "react-image-gallery";
import {
  Magnifier,
  GlassMagnifier,
  SideBySideMagnifier,
  PictureInPictureMagnifier,
  MOUSE_ACTIVATION,
  TOUCH_ACTIVATION,
} from "react-image-magnifiers";

import { headerContext, authContext, loadingContext } from "../../App";

import SimilarProducts from "./similar-products";
import ProductDetail from "./product-detail";

function ProductShowcase() {
  const params = useParams();
  let navigate = useNavigate();
  const [toggleHeader, setToggleHeader] = useContext(headerContext);
  const [isAuthorized, setIsAuthorized] = useContext(authContext);
  const [productObj, setProductObj] = useState({});
  const [displayVariantArr, setDisplayVariantArr] = useState([]);

  const [selectedVariantObj, setSelectedVariantObj] = useState({});
  const [selectedVariantArr, setSelectedVariantArr] = useState([]);

  const [variantOtherThanSelected, setVariantOtherThanSelected] = useState([]);
  const [isLoading, setIsLoading] = useContext(loadingContext);
  const [imageUrl, setImageUrl] = useState(
    "https://drive.google.com/uc?export=view&id=1X5RYQUcNYzexXV_m4YNvjHcXZDpAXHKb"
  );
  const getProduct = async () => {
    setIsLoading(true);
    try {
      const { data: res } = await getProductById(params.id);
      if (res.success) {
        // console.log(res, "response");

        // let tempProductCheckArr=[...res.data.variantArr.filter(el=>el.currentStock>0)];
        // if(tempProductCheckArr.length>0){
        //   let tempSelVariantObj=tempProductCheckArr[0];
        //   // let tempSelectedVariantArr=res.variantArr.
        // }

        setProductObj(res.data);
        let tempSelectedVariantArr = res.variantArr
          .map((el) => {
            let obj = {
              name: el.name,
              _id: el._id,
              variantObj: { ...el.variantArr[0] },
            };
            console.log(obj, "VAR");
            return obj;
          })
          .filter((el) => el.name != "Type");
        setSelectedVariantArr([...tempSelectedVariantArr]);
        let tempObj = res.data.variantArr.find((el) =>
          tempSelectedVariantArr.every((elx) =>
            el.variantIdArr.some((elz) => elz.variantId == elx.variantObj._id)
          )
        );
        // console.log(tempObj, tempSelectedVariantArr, "asd")
        setSelectedVariantObj(tempObj);
        // console.log(res.variantArr,"aaa")
        setDisplayVariantArr([
          ...res.variantArr.filter((el) => el.name != "Type"),
        ]);
        let tempImageUrl =
          tempObj?.imageArr?.length > 0
            ? tempObj?.imageArr[0]?.url
            : ProductImage;
        // console.log(tempImageUrl);
        setImageUrl(tempImageUrl);
        let tempFinalBoxArr = [];
        let tempProductArr = res.data.variantArr;
        for (let el of tempProductArr) {
          let tempVaraintObj = el.variantList.find(
            (elz) => elz.name == "Color"
          );
          if (tempVaraintObj) {
            if (
              tempFinalBoxArr.some(
                (el) => el.variantId == tempVaraintObj.innerVariantId
              )
            ) {
              // tempFinalBoxArr.push(el)
            } else {
              tempFinalBoxArr.push({
                ...el,
                variantId: tempVaraintObj.innerVariantId,
                selectionVariantName: tempVaraintObj.variantName,
              });
            }
          }
        }
        setVariantOtherThanSelected([...tempFinalBoxArr]);
        // console.log(tempFinalBoxArr)
      }
    } catch (error) {
      console.error(error);
    }
    setIsLoading(false);
  };

  const checkVariantSelected = (item) => {
    if (selectedVariantArr.some((el) => el.variantObj._id == item._id)) {
      return true;
    } else {
      return false;
    }
  };

  const handleVariantSelection = (innerVariantObj, outerIndex) => {
    // setIsLoading(true);
    // console.log("selecting variant")
    let tempSelectedVariantArr = [...selectedVariantArr];
    tempSelectedVariantArr[outerIndex].variantObj = innerVariantObj;

    let tempObj = productObj.variantArr.find((el) =>
      tempSelectedVariantArr.every((elx) =>
        el.variantIdArr.some((elz) => elz.variantId == elx.variantObj._id)
      )
    );

    let tempImageUrl =
      tempObj?.imageArr?.length > 0 ? tempObj?.imageArr[0]?.url : ProductImage;
    // console.log(tempImageUrl);
    setImageUrl(tempImageUrl);
    setSelectedVariantArr([...tempSelectedVariantArr]);
    console.log(tempObj, "tempObj");
    setSelectedVariantObj(tempObj);

    // console.log([...productObj.variantArr.filter(el => el._id != tempObj._id)])
    // setIsLoading(false);
  };

  const handleStockCheckForVariant = (id) => {
    if (
      selectedVariantObj.currentStock == 0 &&
      selectedVariantObj.variantIdArr.some((el) => el.variantId == id)
    ) {
      return false;
    } else {
      return true;
    }
  };

  const addToCart = async () => {
    // setIsLoading(true);
    try {
      console.log(isAuthorized);
      if (isAuthorized) {
        let obj = {
          productId: productObj._id,
          variantId: selectedVariantObj._id,
          quantity: 1,
        };
        const { data: res } = await addItemToCart(obj);
        if (res.success) {
          alert(res.message);
          navigate("/cart");
        }
      } else {
        let tempArr = localStorage.getItem("jootelelo-cart");
        console.log(tempArr);
        if (tempArr) {
          tempArr = JSON.parse(tempArr);
          let index = tempArr.findIndex(
            (el) =>
              el.productId == productObj._id &&
              el.variantId == selectedVariantObj._id
          );
          if (index != -1) {
            tempArr[index].quantity = tempArr[index].quantity + 1;
            await localStorage.setItem(
              "jootelelo-cart",
              JSON.stringify(tempArr)
            );
            setIsLoading(false);
          } else {
            let obj = {
              productId: productObj._id,
              variantId: selectedVariantObj._id,
              quantity: 1,
              productObj: productObj,
              variantObj: selectedVariantObj,
            };
            tempArr.push(obj);
            await localStorage.setItem(
              "jootelelo-cart",
              JSON.stringify(tempArr)
            );
            setIsLoading(false);
            navigate("/cart");
          }
        } else {
          // setToggleHeader(true);
          tempArr = [];
          console.log("Not found");
          let obj = {
            productId: productObj._id,
            variantId: selectedVariantObj._id,
            quantity: 1,
            productObj: productObj,
            variantObj: selectedVariantObj,
          };
          tempArr.push(obj);
          await localStorage.setItem("jootelelo-cart", JSON.stringify(tempArr));
          setIsLoading(false);

          navigate("/cart");
        }

        // setToggleHeader(true);
      }
    } catch (error) {
      if (error?.response?.data?.message) {
        alert(error.response.data.message);
      } else {
        alert(error.message);
      }
      console.error(error);
    }
    setIsLoading(false);
  };

  const handleCartRedirect = () => {
    if (isAuthorized) {
      navigate("/cart");
    } else {
      setToggleHeader(true);
    }
  };

  const handleAddTowishlist = async () => {};

  useEffect(() => {
    getProduct();
  }, [params.id]);

  return (
    <>
      <PageBanner name={productObj?.department} />

      <section id="product_images">
        <div className="container">
          <div className="row">
            <div className="col-sm-6">
              {/* <div className="row">
                <div className="col-3">
                  <div className="small_images">
                    {selectedVariantObj?.imageArr?.length > 0 ? (
                      <ul className="list-unstyled">
                        {selectedVariantObj?.imageArr.map((el, index) => {
                          return (
                            <li
                              key={index}
                              style={{ cursor: "pointer" }}
                              onClick={() => setImageUrl(el.url)}
                              className="image_list"
                            >
                              <img
                                src={el.url}
                                className="img-fluid change_image"
                              />
                            </li>
                          );
                        })}
                      </ul>
                    ) : (
                      <ul className="list-unstyled">
                        <li
                          onClick={() => setImageUrl(ProductImage)}
                          className="image_list"
                        >
                          <img
                            src={ProductImage}
                            className="img-fluid change_image"
                            alt=""
                          />
                        </li>
                        <li
                          onClick={() => setImageUrl(ProductImage)}
                          className="image_list"
                        >
                          <img
                            src={ProductImage}
                            className="img-fluid"
                            alt=""
                          />
                        </li>
                      </ul>
                    )}
                  </div>
                </div>
                <div className="col-8 offset-1 pl-0">
                  <div className="big-image">
                    <GlassMagnifier
                      allowOverflow={true}
                      imageSrc={imageUrl}
                      square={true}
                      magnifierBorderColor={"#E2AD07"}
                      // imageAlt="Example"
                      largeImageSrc={imageUrl} // Optional
                    />
                    
                  </div>
                 
                </div>
              </div> */}
              <div className="row">
                {selectedVariantObj &&
                  selectedVariantObj?.imageArr?.length > 0 && (
                    <ImageGallery
                      autoPlay={true}
                      showPlayButton={false}
                      thumbnailPosition="left"
                      items={[
                        ...selectedVariantObj?.imageArr.map((el) => {
                          let obj = {
                            original: el.url,
                            thumbnail: el.url,
                          };
                          return obj;
                        }),
                      ]}
                    />
                  )}
              </div>
            </div>
            <div className="col-sm-6">
              <div className="product_info">
                <div className="extra">
                  <h2 className="product_discription_name">
                    {productObj?.name}
                  </h2>
                  <h2 className="product_discription_name">
                    Brand : {productObj?.brandName}
                  </h2>
                  <p className="product_discription">
                    {selectedVariantObj?.name}
                  </p>
                  <div className="extra">
                    <i className="fas fa-star star"></i>
                    <i className="fas fa-star star"></i>
                    <i className="fas fa-star star"></i>
                    <i className="fas fa-star star"></i>
                    <i className="fas fa-star-half-alt star"></i>
                  </div>
                  <p className="current_price">
                    <span className="last_price">
                      ₹ {selectedVariantObj?.price}
                    </span>{" "}
                    <span className="px-2">
                      {" "}
                      {selectedVariantObj.discount * 100}% Off{" "}
                    </span>{" "}
                    <span className="px-2">
                      ₹ {selectedVariantObj?.realizedValue}{" "}
                    </span>
                  </p>
                  {selectedVariantObj?.currentStock == 0 ? (
                    <p className="current_price"> Sold Out</p>
                  ) : (
                    <p className="current_price">In Stock</p>
                  )}

                  {displayVariantArr.map((el, index) => {
                    return (
                      <div key={el._id} className="product_size">
                        <h5>{el.name}</h5>
                        {`${el.name}`.toLowerCase() == "size" ? (
                          <>
                            <div
                              className="row"
                              style={{ display: "flex", flexDirection: "row" }}
                            >
                              <select
                                className="form-control"
                                onChange={(e) => {
                                  console.log(e.target.value);
                                  handleVariantSelection(
                                    JSON.parse(e.target.value),
                                    index
                                  );
                                }}
                                style={{ width: 100, marginLeft: 10 }}
                              >
                                {el.variantArr.map((elx, indexX) => {
                                  return (
                                    <option
                                      key={elx._id}
                                      value={JSON.stringify(elx)}
                                    >
                                      {elx.name}
                                    </option>

                                    // <div
                                    //   key={elx._id}
                                    //   onClick={() =>
                                    //     handleVariantSelection(elx, index)
                                    //   }
                                    //   className={`product_size_types col-6 col-xl-3 `}
                                    //   style={
                                    //     checkVariantSelected(elx)
                                    //       ? { borderColor: "red", borderWidth: 2 }
                                    //       : { borderColor: "black" }
                                    //   }
                                    // >
                                    //   <div
                                    //     style={{
                                    //       borderRadius: 5,
                                    //       minWidth: 100,
                                    //       paddingInline: 10,
                                    //       borderColor: checkVariantSelected(elx)
                                    //         ? "#E2AD07"
                                    //         : "black",
                                    //       cursor: "pointer",
                                    //       backgroundColor:
                                    //         handleStockCheckForVariant(elx._id)
                                    //           ? "white"
                                    //           : "white",
                                    //     }}
                                    //     className={`product_size_types_div ${
                                    //       !handleStockCheckForVariant(elx._id) &&
                                    //       "strikethrough"
                                    //     }`}
                                    //   >
                                    //     <span className="ml-4">{elx.name}</span>
                                    //   </div>
                                    // </div>
                                  );
                                })}
                              </select>
                            </div>
                          </>
                        ) : `${el.name}`.toLowerCase() == "color" ? (
                          <>
                            <div
                              style={{ display: "flex", flexDirection: "row" }}
                            >
                              {el.variantArr.map((elx, indexX) => {
                                return (
                                  <div
                                    key={elx._id}
                                    onClick={() =>
                                      handleVariantSelection(elx, index)
                                    }
                                    className="product_size_types"
                                    style={
                                      checkVariantSelected(elx)
                                        ? { borderColor: "red", borderWidth: 2 }
                                        : { borderColor: "black" }
                                    }
                                  >
                                    <div
                                      style={{
                                        borderRadius: 50,
                                        minWidth: 50,
                                        paddingInline: 10,
                                        // borderColor: checkVariantSelected(elx)
                                        //   ? "#E2AD07"
                                        //   : "black",
                                        cursor: "pointer",
                                        backgroundColor: elx.colorHex,
                                      }}
                                      className="product_size_types_div "
                                    >
                                      {/* <span className="ml-4">{elx.name}{elx.colorHex}</span> */}
                                    </div>
                                  </div>
                                );
                              })}
                            </div>
                          </>
                        ) : (
                          <>
                            <div
                              style={{ display: "flex", flexDirection: "row" }}
                            >
                              {el.variantArr.map((elx, indexX) => {
                                return (
                                  <div
                                    key={elx._id}
                                    onClick={() =>
                                      handleVariantSelection(elx, index)
                                    }
                                    className="product_size_types"
                                    style={
                                      checkVariantSelected(elx)
                                        ? { borderColor: "red", borderWidth: 2 }
                                        : { borderColor: "black" }
                                    }
                                  >
                                    <div
                                      style={{
                                        borderRadius: 5,
                                        minWidth: 100,
                                        paddingInline: 10,
                                        borderColor: checkVariantSelected(elx)
                                          ? "#E2AD07"
                                          : "black",
                                        cursor: "pointer",
                                      }}
                                      className="product_size_types_div "
                                    >
                                      <span className="ml-4">{elx.name}</span>
                                    </div>
                                  </div>
                                );
                              })}
                            </div>
                          </>
                        )}
                      </div>
                    );
                  })}
                  <button
                    disabled={selectedVariantObj?.currentStock == 0}
                    style={
                      selectedVariantObj?.currentStock == 0
                        ? { backgroundColor: "grey" }
                        : { backgroundColor: "#516EAE" }
                    }
                    onClick={() => addToCart()}
                    className="quantity_submitbutton mt-4"
                  >
                    Add to cart
                  </button>
                  {isAuthorized ? (
                    <button
                      style={{ marginLeft: 20 }}
                      onClick={() => handleAddTowishlist()}
                      className="quantity_submitbutton ml-2"
                    >
                      Add to wishlist
                    </button>
                  ) : (
                    <></>
                  )}
                  {/* <h4 className="mt-4">Description</h4>
                  <p className="product_discription">
                    {selectedVariantObj?.ProductDescription}
                  </p>
                  <h4>Specifications</h4>
                  <p className="product_discription">
                    Upper : {selectedVariantObj?.Upper}
                  </p>
                  <p className="product_discription">
                    Sole : {selectedVariantObj?.Sole}
                  </p>
                  <p className="product_discription">
                    Insole : {selectedVariantObj?.Insole}
                  </p>
                  <p className="product_discription">
                    Care Instructions :{selectedVariantObj?.CareInstruction}
                  </p> */}
                  {/* <h4>Sole</h4>
                  <h4>Insole</h4>
                  <h4>Care Instructions</h4> */}

                  {/* <input disabled={selectedVariantObj?.currentStock==0}  type="submit" onClick={()=>addToCart()}  value="Add to cart" /> */}
                  {/* <div className="wishlist">
                    <p className="wishlist_text">Add to wishlist :</p>
                    <i className="far fa-heart"></i>
                  </div> */}
                  {/* <div className="social_share">
                    <ul className="list_style">
                      <li>Categories:</li>
                      <li>Shoes,</li>
                      <li>sneaker</li>
                    </ul>
                  </div> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <ProductDetail variantObj={selectedVariantObj} />

      <SimilarProducts productId={params.id} />
    </>
  );
}

export default ProductShowcase;
