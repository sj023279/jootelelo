import React, { useEffect, useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  addAddress,
  checkPincodeArea,
  getAddressesForUser,
} from "../services/Address";
import { getUserCart, removeItemFromCart } from "../services/user";
import PageBanner from "./page-banner";
import $ from "jquery";
import {
  createCodOrder,
  createOrder,
  createWalletOrder,
  orderCallback,
} from "../services/Order";
import { authContext, headerContext } from "../App";
import LogoWhite from "../images/jootelelo2.png";
import { getAllCoupons } from "../services/coupon";

function Cart() {
  const [cartObj, setCartObj] = useState({});

  const [toggleHeader, setToggleHeader] = useContext(headerContext);
  const [addressStepper, setAddressStepper] = useState(0);
  const [pincode, setPincode] = useState("");
  const [name, setName] = useState("");
  const [mobile, setMobile] = useState("");
  const [line1, setLine1] = useState("");
  const [line2, setLine2] = useState("");
  const [city, setCity] = useState("");
  const [adstate, setAdState] = useState("");
  const [deliveryCharge, setDeliveryCharge] = useState(0);
  const [subTotalAmount, setSubTotalAmount] = useState(0);
  const [couponDiscountAmount, setCouponDiscountAmount] = useState(0);
  const [totalDisountAmount, setTotalDisountAmount] = useState(0);
  const [totalWithPrice, setTotalWithPrice] = useState(0);
  const [isAuthorized, setIsAuthorized] = useContext(authContext);

  const [addressArr, setAddressArr] = useState([]);

  const [selectedAddress, setSelectedAddress] = useState("");

  const [paymentMode, setPaymentMode] = useState(1);
  const [couponArr, setCouponArr] = useState([]);
  const [selectedCouponObj, setSelectedCouponObj] = useState({});
  const navigate = useNavigate();
  const getCart = async () => {
    try {
      let tokenCheck = localStorage.getItem("TOKEN");

      console.log(isAuthorized, "AUTHORIZED");
      if (tokenCheck || isAuthorized) {
        const { data: res } = await getUserCart();
        if (res.success) {
          console.log(res.data);
          let tempObj = res.data;
          tempObj.productArr = tempObj.productArr.map((item) => {
            let obj = {
              ...item,
              variantObj: {
                ...item.variantObj,
                realizedValue:
                  item.variantObj.price -
                  item.variantObj.price * item.variantObj.discount,
              },
            };
            return obj;
          });
          setCartObj(res.data);
          getAddress();

          calculateSubTotalAmount(res?.data?.productArr);
        }
      } else {
        let tempArr = localStorage.getItem("jootelelo-cart");
        if (tempArr) {
          tempArr = JSON.parse(tempArr);
          console.log(tempArr);
          setCartObj({ productArr: tempArr });
          calculateSubTotalAmount(
            tempArr.map((el) => ({
              ...el,
              freeQuantity: 0,
              paidQuantity: el.quantity,
            }))
          );
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const removeItem = async (item) => {
    try {
      if (isAuthorized) {
        let obj = {
          productId: item.productId,
          variantId: item.variantId,
        };
        const { data: res } = await removeItemFromCart(obj);
        if (res.success) {
          getCart();
        }
      } else {
        let tempArr = localStorage.getItem("jootelelo-cart");
        if (tempArr) {
          tempArr = JSON.parse(tempArr);

          tempArr = tempArr.filter(
            (el) =>
              el.item.productId != el.productId &&
              el.item.variantId != el.variantId
          );
          console.log(tempArr);
          setCartObj({ productArr: { ...tempArr } });
          localStorage.setItem("jootelelo-cart", JSON.stringify(tempArr));
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const getAddress = async () => {
    try {
      const { data: res } = await getAddressesForUser();
      if (res.success) {
        setAddressArr(res.data);
        // setSelectedAddress
      }
    } catch (error) {
      console.error(error);
    }
  };

  const calculateSubTotalAmount = (arr) => {
    if (arr?.length > 0) {
      let tempArr = [...arr];

      let finalAmount = tempArr.reduce(
        (acc, el) =>
          acc + el?.variantObj?.realizedValue * (el.quantity - el.freeQuantity),
        0
      );
      let tempTotalDiscountAmount = tempArr.reduce(
        (acc, el) =>
          acc +
          (el?.variantObj?.price - el?.variantObj?.realizedValue) *
            (el.quantity - el.freeQuantity),
        0
      );
      let finalAmountWithPrice = tempArr.reduce(
        (acc, el) =>
          acc + el?.variantObj?.price * (el.quantity - el.freeQuantity),
        0
      );
      setTotalWithPrice(finalAmountWithPrice);
      console.log(finalAmount);
      setSubTotalAmount(finalAmount);
      setTotalDisountAmount(tempTotalDiscountAmount);
    }
  };

  const handleNewAdderssAdd = () => {
    // $('.addressForm').removeClass("display_block");
    // navigate("/account/address")
  };

  const handleAdd = async () => {
    try {
      if (
        name != "" &&
        line1 != "" &&
        line2 != "" &&
        city != "" &&
        adstate != "" &&
        mobile != "" &&
        pincode != ""
      ) {
        $(".register_form").removeClass("display_block");
        let obj = {
          name,
          line1,
          phone: mobile,
          line2,
          city,
          state: adstate,
          mobile,
          pincode,
        };
        const { data: res } = await addAddress(obj);
        if (res.success) {
          alert(res.message);
          // navigation.navigate('Address')
        }
      } else {
        alert("Please fill all the fields");
      }
    } catch (error) {
      console.error(error);
      console.error(error);
      if (error?.response?.data?.message) {
        alert(error.response.data.message);
      } else {
        alert(error.message);
      }
    }
  };

  const handlePincodeCheck = async () => {
    try {
      const { data: res } = await checkPincodeArea(pincode);
      console.log(res);
      if (res.success) {
        setAddressStepper(1);
      }
      // alert(res.message);
      // navigation.navigate('Address')
    } catch (error) {
      console.error(error);
      if (error?.response?.data?.message) {
        alert(error.response.data.message);
      } else {
        alert(error.message);
      }
    }
  };

  function loadScript(src) {
    return new Promise((resolve) => {
      const script = document.createElement("script");
      script.src = src;
      script.onload = () => {
        resolve(true);
      };
      script.onerror = () => {
        resolve(false);
      };
      document.body.appendChild(script);
    });
  }

  async function displayRazorpay(obj, orderId) {
    const res = await loadScript(
      "https://checkout.razorpay.com/v1/checkout.js"
    );

    if (!res) {
      alert("Razorpay SDK failed to load. Are you online?");
      return;
    }

    const options = {
      key: "rzp_live_xNhVXjsOtTSPxi", // Enter the Key ID generated from the Dashboard
      amount: obj.amount,
      currency: obj.currency,
      name: "Jootelelo",
      description: "Order",
      // image: { logo },
      order_id: obj.id,
      handler: async function (response) {
        const data = {
          orderCreationId: obj.id,
          razorpayPaymentId: response.razorpay_payment_id,
          razorpayOrderId: response.razorpay_order_id,
          razorpaySignature: response.razorpay_signature,
        };

        const serialize = function (obj) {
          var str = [];
          for (var p in obj)
            if (obj.hasOwnProperty(p)) {
              str.push(
                encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])
              );
            }
          return str.join("&");
        };
        let { data: res } = await orderCallback(serialize(obj), orderId);
        if (res) {
          navigate("/account/order");
          alert(res.message);
        }
      },

      theme: {
        color: "#61dafb",
      },
    };

    const paymentObject = new window.Razorpay(options);
    paymentObject.open();
  }
  const handleCheckout = async () => {
    try {
      if (selectedAddress != "") {
        let addressObj = addressArr.find((el) => el._id == selectedAddress);
        let obj = {
          addressObj,
          couponId: cartObj.couponId,
        };
        const res = await createOrder(obj);
        if (res.data.success) {
          console.log(JSON.stringify(res.data, null, 2));
          displayRazorpay(res.data.data, res.data.orderId);
        }
      } else {
        alert("Please Select Delivery Address");
      }
    } catch (error) {
      console.error(error);
    }
  };
  const handleWalletCheckout = async () => {
    try {
      if (selectedAddress != "") {
        let addressObj = addressArr.find((el) => el._id == selectedAddress);
        let obj = {
          addressObj,
          couponId: cartObj.couponId,
        };
        const res = await createWalletOrder(obj);
        if (res.data.success) {
          console.log(JSON.stringify(res.data, null, 2));
          alert(res.data.message);
          navigate("/account/order");
        }
      } else {
        alert("Please Select Delivery Address");
      }
    } catch (error) {
      if (error.response.data.message) {
        alert(error.response.data.message);
      } else {
        alert(error.message);
      }
      console.error(error);
    }
  };
  const handleCodCheckout = async () => {
    try {
      if (selectedAddress != "") {
        let addressObj = addressArr.find((el) => el._id == selectedAddress);
        let obj = {
          addressObj,
          couponId: cartObj.couponId,
        };
        const res = await createCodOrder(obj);
        if (res.data.success) {
          navigate("/account/order");

          alert(res.data.message);
        }
      } else {
        alert("Please Select Delivery Address");
      }
    } catch (error) {
      if (error.response.data.message) {
        alert(error.response.data.message);
      } else {
        alert(error.message);
      }
      console.error(error);
    }
  };

  const getCoupons = async () => {
    try {
      const { data: res } = await getAllCoupons();
      if (res) {
        setCouponArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleModeSelection = () => {
    if (paymentMode == 1) {
      handleWalletCheckout();
    } else if (paymentMode == 2) {
      handleCheckout();
    } else {
      handleCodCheckout();
    }
  };

  const handleLogin = () => {
    setToggleHeader(true);
  };
  const handleCouponSelection = async () => {
    try {
      // const {data:res}=await
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getCart();
    // getCoupons();
    return () => {
      setCartObj({});
      setAddressArr([]);
    };
  }, [toggleHeader]);

  return (
    <>
      <PageBanner name="Your Shoping Cart" />

      <section className="cartmain">
        <div className="container">
          <div className="card">
            <div className="row">
              <div className="col-md-8 cart">
                <div className="title border-bottom">
                  <div className="row">
                    <div className="col">
                      <h4>
                        <b>Shopping Cart</b>
                      </h4>
                    </div>
                    <div className="col align-self-center text-right text-muted">
                      {cartObj?.productArr?.length} items
                    </div>
                  </div>
                </div>

                {cartObj &&
                  cartObj.userId != "" &&
                  cartObj?.productArr?.length > 0 &&
                  cartObj?.productArr?.map((item, index) => {
                    console.log("item", item);
                    return (
                      <div key={index} className="cart-item border-bottom">
                        <div className="row main align-items-center">
                          <div className="col-4 d-flex align-items-center">
                            <div className="cart-item-img overflow-hidden">
                              <img
                                className="img-fluid"
                                src={
                                  item?.variantObj
                                    ? item?.variantObj?.imageArr[0]?.url
                                    : "assets/images/bestseller_shoe_five.webp"
                                }
                                alt=""
                              />
                            </div>
                            <div className="cart-content">
                              <div className="text-muted">
                                {item?.productObj?.name}
                              </div>
                              <div>{item?.variantObj?.name}</div>
                            </div>
                          </div>

                          <div className="col-2">
                            <div className="cart-price">
                              <p className="m-0">
                                ₹ {Math.round(item?.variantObj?.realizedValue)}{" "}
                                <s>₹ {Math.round(item?.variantObj?.price)}</s>
                              </p>
                            </div>
                          </div>
                          <div className="col-2">
                            <div className="cart-price">
                              <p className="m-0">
                                {item?.variantObj?.discount * 100}% Off
                              </p>
                            </div>
                          </div>
                          <div className="col-1">
                            <span
                              className="close"
                              onClick={() => removeItem(item)}
                            >
                              &#10005;
                            </span>
                          </div>
                        </div>
                      </div>
                    );
                  })}

                <div className="back-to-shop d-flex align-items-center justify-content-between mt-3">
                  <Link
                    to="/products"
                    className="text-decoration-none text-black"
                  >
                    <i className="fas fa-long-arrow-alt-left"></i>
                    <span>Back to shop</span>
                  </Link>
                  <div
                    data-bs-toggle="modal"
                    data-bs-target="#coupon-modal"
                    className="text-decoration-none text-black"
                  >
                    <i className="fas fa-long-arrow-alt-right"></i>
                    <span>View Coupons</span>
                  </div>
                </div>
              </div>
              {cartObj?.productArr && cartObj?.productArr.length > 0 && (
                <div className="col-md-4 summary">
                  <div>
                    <h5>
                      <b>Summary</b>
                    </h5>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col">
                      ITEMS {cartObj?.productArr?.length}
                    </div>
                    <div className="col text-right">
                      ₹{Math.round(totalWithPrice)}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">Discount Amount</div>
                    <div className="col text-right">
                      - ₹{Math.round(totalWithPrice - subTotalAmount)}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">Sub Total Amount</div>
                    <div className="col text-right">
                      ₹{Math.round(subTotalAmount)}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">Delivery Charges</div>
                    <div className="col text-right">
                      ₹{Math.round(deliveryCharge)}{" "}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">Payment Mode</div>
                  </div>
                  <div className="row mt-2">
                    <select
                      onChange={(e) => setPaymentMode(e.target.value)}
                      class="form-select"
                      style={{ width: "90%" }}
                      aria-label="Default select example"
                    >
                      <option selected>Please Select Payment Mode</option>
                      <option value="1">Jootelelo Wallet</option>
                      <option value="2">Card/Netbanking/E-wallet</option>
                      <option value="3">Cash on delivery</option>
                    </select>
                  </div>

                  <div
                    className="total_price row"
                    style={{
                      borderTop: "1px solid rgba(0,0,0,.1)",
                      padding: "2vh 0",
                    }}
                  >
                    <div className="col">TOTAL PRICE</div>
                    <div className="col text-right">
                      ₹{Math.round(subTotalAmount + deliveryCharge)}{" "}
                    </div>
                  </div>
                  {isAuthorized ? (
                    <div className="extra_center mt-2">
                      <button
                        className="view_cart"
                        data-bs-toggle="modal"
                        data-bs-target="#address-option"
                      >
                        checkout
                      </button>
                    </div>
                  ) : (
                    <div className="extra_center mt-2">
                      <button
                        className="view_cart"
                        onClick={() => handleLogin()}
                      >
                        Login
                      </button>
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </section>

      <div
        className="modal fade addressForm"
        id="address-option"
        aria-labelledby="address-option"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="text-end">
              <button
                type="button"
                className="btn-close model_close btn-close-white"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="col-sm-12">
                <div className="model_logo_div">
                  <img
                    alt=""
                    src="assets/images/newlogo.png"
                    className="model_logo"
                  />
                </div>
                <div className="model_signin">
                  <p>Select An Address</p>
                  <form action="">
                    {addressArr.map((el, index) => {
                      return (
                        <div key={index} className="form-check">
                          <input
                            className="form-check-input"
                            type="radio"
                            checked={selectedAddress == el._id}
                            onClick={() => setSelectedAddress(el._id)}
                            id="address1"
                          />
                          <label className="form-check-label" for="address1">
                            <p className="m-0">
                              {el.line1}
                              {el.line2},{el.city},{el.state}-{el.pincode}
                            </p>
                            <p className="m-0">Phone no - {el.phone}</p>
                          </label>
                        </div>
                      );
                    })}
                  </form>
                  <button
                    data-bs-target="#exampleModal"
                    data-bs-toggle="modal"
                    aria-label="Close"
                    // onClick={() => handleNewAdderssAdd()}
                    className="btn check_out mx-2"
                  >
                    Add New Address
                  </button>
                  <button
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    onClick={() => handleModeSelection()}
                    className="btn check_out mx-2"
                  >
                    Checkout
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="modal fade addressForm"
        id="coupon-modal"
        aria-labelledby="coupon-modal"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="text-end">
              <button
                type="button"
                className="btn-close model_close btn-close-white"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="col-sm-12">
                <div className="model_logo_div">
                  <img alt="" src={LogoWhite} className="model_logo" />
                </div>
                <div className="model_signin">
                  {couponArr && couponArr.length > 0 && (
                    <>
                      <p>Coupons</p>
                      <form action="">
                        {couponArr.map((el, index) => {
                          return (
                            <div key={index} className="form-check">
                              <input
                                className="form-check-input"
                                type="radio"
                                checked={selectedCouponObj == el._id}
                                onClick={() => setSelectedCouponObj(el)}
                                id="address1"
                              />
                              <label
                                className="form-check-label"
                                for="address1"
                              >
                                <p className="m-0">{el.name}</p>
                                <p className="m-0">Phone no - {el.phone}</p>
                              </label>
                            </div>
                          );
                        })}
                      </form>

                      <button
                        data-bs-dismiss="modal"
                        aria-label="Close"
                        onClick={() => handleCouponSelection()}
                        className="btn check_out mx-2"
                      >
                        Apply
                      </button>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="modal login-form fade"
        id="exampleModal"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="text-end">
              <button
                type="button"
                onClick={() => setAddressStepper(0)}
                className="btn-close model_close btn-close-white"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="col-sm-12">
                <div className="model_logo_div">
                  <img
                    alt=""
                    src="assets/images/newlogo.png"
                    className="model_logo"
                  />
                </div>
                <div className="model_signin">
                  {addressStepper == 0 ? (
                    <>
                      <p className="text-center">Check Pincode</p>
                      <form
                        style={{ display: "block" }}
                        className="register_form"
                      >
                        <input
                          className="form-control"
                          type="text"
                          name="line2"
                          placeholder="Pincode"
                          value={pincode}
                          onChange={(e) => setPincode(e.target.value)}
                        />

                        <div
                          style={{
                            display: "flex",
                            justifyContent: "center",
                          }}
                        >
                          <button
                            type="button"
                            className="sign_in"
                            style={{ width: "100%" }}
                            onClick={() => handlePincodeCheck()}
                          >
                            Submit
                          </button>
                        </div>
                      </form>
                    </>
                  ) : (
                    <>
                      <p className="text-center">Add Address</p>
                      <form
                        style={{ display: "block" }}
                        className="register_form"
                      >
                        <input
                          className="form-control"
                          type="text"
                          name="phone"
                          placeholder="Name"
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                        />
                        <input
                          className="form-control"
                          type="text"
                          name="number"
                          placeholder="Phone"
                          value={mobile}
                          onChange={(e) => setMobile(e.target.value)}
                        />
                        <input
                          className="form-control"
                          type="text"
                          name="addressline1"
                          placeholder="Address Line 1"
                          value={line1}
                          onChange={(e) => setLine1(e.target.value)}
                        />
                        <input
                          className="form-control"
                          type="text"
                          name="line2"
                          placeholder="Address Line 2"
                          value={line2}
                          onChange={(e) => setLine2(e.target.value)}
                        />
                        <input
                          className="form-control"
                          type="text"
                          name="line2"
                          placeholder="City"
                          value={city}
                          onChange={(e) => setCity(e.target.value)}
                        />

                        <input
                          className="form-control"
                          type="text"
                          name="line2"
                          placeholder="State"
                          value={adstate}
                          onChange={(e) => setAdState(e.target.value)}
                        />

                        <input
                          className="form-control"
                          type="text"
                          name="line2"
                          placeholder="Pincode"
                          value={pincode}
                          onChange={(e) => setPincode(e.target.value)}
                        />

                        <div
                          style={{
                            display: "flex",
                            justifyContent: "center",
                          }}
                        >
                          <button
                            className="sign_in"
                            style={{ width: "100%" }}
                            onClick={() => handleAdd()}
                          >
                            Submit
                          </button>
                        </div>
                      </form>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Cart;
