import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import PageBanner from "../page-banner";
import Account from "../account";
import LogoWhite from "../../images/jootelelo2.png";

import {
  createWalletRecharge,
  getUserById,
  walletOrderPaymentCallback,
} from "../../services/user";
import { createWalletOrder } from "../../services/Order";

function AccountProfile() {
  const [userObj, setUserObj] = useState({});

  const [amount, setAmount] = useState(0);
  const getUser = async () => {
    try {
      const { data: res } = await getUserById();
      if (res.success) {
        console.log(res.data);
        setUserObj(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  function loadScript(src) {
    return new Promise((resolve) => {
      const script = document.createElement("script");
      script.src = src;
      script.onload = () => {
        resolve(true);
      };
      script.onerror = () => {
        resolve(false);
      };
      document.body.appendChild(script);
    });
  }
  useEffect(() => {
    getUser();
  }, []);

  async function displayRazorpay(obj, orderId) {
    const res = await loadScript(
      "https://checkout.razorpay.com/v1/checkout.js"
    );

    if (!res) {
      alert("Razorpay SDK failed to load. Are you online?");
      return;
    }

    const options = {
      key: "rzp_live_xNhVXjsOtTSPxi", // Enter the Key ID generated from the Dashboard
      amount: obj.amount,
      currency: obj.currency,
      name: "Jooteylelo",
      description: "Order",
      // image: { logo },
      order_id: obj.id,
      handler: async function (response) {
        const data = {
          orderCreationId: obj.id,
          razorpayPaymentId: response.razorpay_payment_id,
          razorpayOrderId: response.razorpay_order_id,
          razorpaySignature: response.razorpay_signature,
        };

        const serialize = function (obj) {
          var str = [];
          for (var p in obj)
            if (obj.hasOwnProperty(p)) {
              str.push(
                encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])
              );
            }
          return str.join("&");
        };
        let { data: res } = await walletOrderPaymentCallback(
          serialize(obj),
          orderId
        );
        if (res) {
          alert(res.message);
          getUser();
        }
      },

      theme: {
        color: "#61dafb",
      },
    };

    const paymentObject = new window.Razorpay(options);
    paymentObject.open();
  }
  const handleWalletRecharge = async () => {
    try {
      let obj = {
        rechargeAmount: amount,
      };
      const res = await createWalletRecharge(obj);
      if (res) {
        console.log(res);
        displayRazorpay(res.data.data, res.data.orderId);
      }
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <>
      <PageBanner name="My profile" />

      <section id="mainaccount_sec">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-3">
              <Account active_account={true} />
            </div>
            <div className="col-12 col-md-9">
              <div className="right_account_details">
                <h4>Account Information</h4>
                <div className="main_right_details accountinfo">
                  <div className="accountinfo_main">
                    <ul className="user_details m-0">
                      <li>
                        <i className="fas fa-user"></i> Your Name :{" "}
                        {userObj?.name}
                      </li>
                      <li>
                        <i className="far fa-envelope"></i>Email :{" "}
                        {userObj?.email}
                      </li>
                      <li>
                        <i className="fas fa-phone"></i> Phone :{" "}
                        {userObj?.phone}
                      </li>
                      <li>
                        <i className="fas fa-key"></i> Wallet : ₹{" "}
                        {userObj?.walletObj?.wallet}{" "}
                        <button
                          data-bs-toggle="modal"
                          data-bs-target="#address-option"
                          className="btn check_out mx-2"
                        >
                          Recharge
                        </button>{" "}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div
        className="modal fade addressForm"
        id="address-option"
        aria-labelledby="address-option"
        aria-hidden="true"
      >
        {/* <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="text-end">
              <button
                type="button"
                className="btn-close model_close btn-close-white"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="col-sm-12">
                <div className="model_logo_div">
                  <img
                    alt=""
                    src="assets/images/newlogo.png"
                    className="model_logo"
                  />
                </div>
                <div className="model_signin">
                  <p className="text-center">Enter Amount</p>
                  <input
                    type="number"
                    placeholder="Enter Amount"
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                    className="form-control"
                  />
               

                  <button
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    onClick={() => handleWalletRecharge()}
                    className="btn check_out mx-2 mt-4 d-flex align-self-center"
                  >
                    Recharge
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div> */}
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="text-end">
              <button
                type="button"
                className="btn-close model_close btn-close-white"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="col-sm-12">
                <div className="model_logo_div d-flex align-items-center justify-content-center">
                  <img
                    alt=""
                    src={LogoWhite}
                    style={{ height: 100, width: 220 }}
                  />
                </div>
                <div className="model_signin">
                  <p>Enter Amount</p>
                  <form className="sign_in_form">
                    <input
                      type="number"
                      placeholder="Enter Amount"
                      value={amount}
                      style={{ width: "100%" }}
                      onChange={(e) => setAmount(e.target.value)}
                      className="form-control"
                    />
                    <button
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      type="button"
                      onClick={() => handleWalletRecharge()}
                      className="btn check_out mx-2 mt-4 d-flex align-self-center"
                    >
                      Proceed
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default AccountProfile;
