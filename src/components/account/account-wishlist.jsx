import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import ProductImage from "../../images/bestseller_shoe_ninetwo.webp";
import { getAllWishlist } from "../../services/wishlist";
import Account from "../account";
import PageBanner from "../page-banner";

function AccountWishlist() {
  const [wishlistArr, setWishlistArr] = useState([]);

  const navigation = useNavigate();

  const getWishlist = async () => {
    try {
      const { data: res } = await getAllWishlist();
      if (res.data) {
        console.log(res.data);
        setWishlistArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleRedirect = (id) => {
    navigation(`/single-product/${id}`);
  };

  useEffect(() => {
    getWishlist();
  }, []);

  return (
    <>
      <PageBanner name="My Wishlist" />

      <section id="mainaccount_sec">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-3">
              <Account active_account={true} />
            </div>
            <div className="col-12 col-md-9">
              <div className="right_account_details">
                <h4>My Wishlist</h4>
                <div className="main_right_details mywishlist">
                  <div className="wishlist_main">
                    <div className="panel-body">
                      <div className="table-responsive">
                        <table className="table table-borderless my-wishlist-table">
                          <thead>
                            <tr>
                              <th>IMAGE</th>
                              <th>PRODUCT NAME</th>
                              <th>PRICE</th>
                              <th>Discount</th>
                              <th>Total</th>

                              {/* <th>ACTION</th> */}
                            </tr>
                          </thead>
                          <tbody>
                            {wishlistArr.map((el, index) => {
                              return (
                                <tr>
                                  <td>
                                    <div className="product-image">
                                      <img
                                        src={el?.variantObj?.imageArr[0].url}
                                        alt="product-image"
                                        className=""
                                        width="80px"
                                        height="auto"
                                      />
                                    </div>
                                  </td>
                                  <td
                                    onClick={() => handleRedirect(el.productId)}
                                  >
                                    {el.productObj.name}({el.variantObj.name})
                                  </td>
                                  <td>
                                    <span className="product-price">
                                      ₹{el.variantObj?.price}
                                    </span>
                                  </td>
                                  <td>
                                    <span className="wishlist-status-link badge bg-success">
                                      {Math.round(el.variantObj.discount * 100)}
                                      %
                                    </span>
                                  </td>
                                  <td>
                                    <span className="product-price">
                                      ₹
                                      {el.variantObj.price -
                                        Math.round(
                                          el.variantObj.discount *
                                            el.variantObj.price
                                        )}
                                    </span>
                                  </td>

                                  {/* <td><button className="btn btn-delete">X</button></td> */}
                                </tr>
                              );
                            })}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default AccountWishlist;
