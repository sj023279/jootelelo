import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import PageBanner from "../page-banner";
import Account from "../account";
import { cancelOrderById, getAllOrders } from "../../services/Order";

import OrderImage from "../../images/newlogo.png";

function AccountOrder() {
  const [orderArr, setOrderArr] = useState([]);

  const navigate = useNavigate();
  const getOrder = async () => {
    try {
      const { data: res } = await getAllOrders();
      if (res.success) {
        console.log(res.data);
        setOrderArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleOrderCancel = async (id) => {
    try {
      if (window.confirm("Are you sure you want to cancel this order?")) {
        const { data: res } = await cancelOrderById(id);
        if (res.success) {
          alert(res.message);
        }
      }
    } catch (error) {
      console.error(error);
      alert(error.message);
    }
  };

  const handleDetailRedirect = (id) => {
    navigate(`/view-order/${id}`);
  };

  useEffect(() => {
    getOrder();
  }, []);

  return (
    <>
      <PageBanner name="My Orders" />

      <section id="mainaccount_sec">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-3">
              <Account active_account={true} />
            </div>
            <div className="col-12 col-md-9">
              <div className="right_account_details">
                <h4>My Order</h4>
                <div className="main_right_details myorder">
                  {orderArr.map((el, index) => {
                    return (
                      <div className="card mb-4">
                        <div className="card-header">
                          <div className="row">
                            <div className="col-md-2">
                              <span>Order : </span> <br />{" "}
                              <span>
                                {new Date(el?.createdAt).toDateString()}
                              </span>
                            </div>
                            <div className="col-md-4">
                              <span>Total</span> <br />{" "}
                              <span>₹{el?.payableAmount}</span>
                            </div>
                            <div className="col-md-4">
                              <span>Ship To</span> <br />{" "}
                              <span>{el?.addressObj?.name}</span>
                            </div>
                            <div className="col-md-2 text-end">
                              <span>ORDER Id: {el?.invoiceNumber} </span> <br />
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="prCard row mb-2">
                            {/* <div
                              className="col-md-1"
                              style={{
                                backgroundColor: "black",
                                borderRadius: 100,
                                height: 70,
                                display: "flex",
                                alignItems: "center",
                              }}
                            >
                              <img
                                src={OrderImage}
                                width="50"
                                height="50"
                                alt="product image"
                                style={{ alignSelf: "center" }}
                              />
                            </div> */}
                            <div className="col-md-9">
                              <h6>
                                Delivery Address : {el?.addressObj?.line1},
                                {el?.addressObj?.line2},{el?.addressObj?.city},
                                {el?.addressObj?.state}-
                                {el?.addressObj?.pincode}
                              </h6>{" "}
                              <span className="small">
                                Qty :{" "}
                                {el?.cartObj?.productArr?.reduce(
                                  (acc, el, i) => acc + el.quantity,
                                  0
                                )}
                              </span>
                            </div>
                            {/* <div className="col-md-4"></div> */}
                            <div className="col-md-3 text_right">
                              <button
                                onClick={() => handleDetailRedirect(el._id)}
                                className="btn-sm btn btn-dark mb-2"
                              >
                                View Order
                              </button>
                              {el.statusObj.status != "CANCELLED" &&
                                el.statusObj.status != "DELIVERED" && (
                                  <button
                                    onClick={() => handleOrderCancel(el._id)}
                                    className="btn-sm btn btn-danger mb-2"
                                  >
                                    Cancel Order
                                  </button>
                                )}
                              {/* <Link to="/view-order" >View Order</Link> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default AccountOrder;
