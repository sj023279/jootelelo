import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import PageBanner from "../page-banner";
import Account from "../account";
import {
  addAddress,
  checkPincodeArea,
  getAddressesForUser,
} from "../../services/Address";

import $ from "jquery";

function AccountAddress() {
  const [addressArr, setAddressArr] = useState([]);

  const [name, setName] = useState("");
  const [mobile, setMobile] = useState("");
  const [line1, setLine1] = useState("");
  const [line2, setLine2] = useState("");
  const [city, setCity] = useState("");
  const [adstate, setAdState] = useState("");
  const [pincode, setPincode] = useState("");

  const [addressStepper, setAddressStepper] = useState(0);
  const getAddress = async () => {
    try {
      const { data: res } = await getAddressesForUser();
      if (res.success) {
        console.log(res.data);
        setAddressArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };
  const handleAdd = async () => {
    try {
      if (
        name != "" &&
        line1 != "" &&
        line2 != "" &&
        city != "" &&
        adstate != "" &&
        mobile != "" &&
        pincode != ""
      ) {
        $(".register_form").removeClass("display_block");
        let obj = {
          name,
          line1,
          phone: mobile,
          line2,
          city,
          state: adstate,
          mobile,
          pincode,
        };
        const { data: res } = await addAddress(obj);
        if (res.success) {
          alert(res.message);
          // navigation.navigate('Address')
        }
      } else {
        alert("Please fill all the fields");
      }
    } catch (error) {
      console.error(error);
      console.error(error);
      if (error?.response?.data?.message) {
        alert(error.response.data.message);
      } else {
        alert(error.message);
      }
    }
  };

  const handlePincodeCheck = async () => {
    try {
      const { data: res } = await checkPincodeArea(pincode);
      console.log(res);
      if (res.success) {
        setAddressStepper(1);
      }
      // alert(res.message);
      // navigation.navigate('Address')
    } catch (error) {
      console.error(error);
      if (error?.response?.data?.message) {
        alert(error.response.data.message);
      } else {
        alert(error.message);
      }
    }
  };

  useEffect(() => {
    getAddress();
  }, []);
  return (
    <>
      <PageBanner name="My Address" />

      <section id="mainaccount_sec">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-3">
              <Account active_account={true} />
            </div>
            <div className="col-12 col-md-9">
              <div className="right_account_details">
                <h4>My Address</h4>
                <div className="main_right_details myaddresses">
                  <div className="wishlist_main">
                    <div className="row">
                      {addressArr.map((el, index) => {
                        return (
                          <div key={index} className="col-xl-6 col-lg-9 d-flex">
                            <address className="address-card">
                              <div className="address-card-data">
                                <div className="card">
                                  <div className="card-body">
                                    <span className="mb-3 namein_addres">
                                      {el.name}
                                    </span>
                                    <br />{" "}
                                    <span style={{ fontWeight: "400" }}>
                                      {el.line1}
                                    </span>
                                    <br />{" "}
                                    <span style={{ fontWeight: "400" }}>
                                      {el.line2}
                                    </span>
                                    <br />{" "}
                                    <span style={{ fontWeight: "400" }}>
                                      {el.city},{el.state}-{el.pincode}
                                    </span>
                                    {/* <span style={{ fontWeight: '400' }}>India</span> */}
                                  </div>
                                </div>
                              </div>
                              <div className="address-card-actions mt-3">
                                {/* <button
                                  type="button"
                                  className="btn btn-primary-custom-o btn-primary-custom-sm btn-edit-address"
                                  style={{ fontWeight: "400" }}
                                >
                                  Edit
                                </button> */}
                                <button
                                  type="button"
                                  className="btn btn-danger-custom btn-primary-custom-sm btn-delete-address"
                                  style={{ fontWeight: "400" }}
                                >
                                  Delete
                                </button>
                              </div>
                            </address>
                          </div>
                        );
                      })}

                      <div className="col-md-12 mt-3">
                        <button
                          type="button"
                          data-bs-toggle="modal"
                          onClick={() =>
                            $(".register_form").addClass("display_block")
                          }
                          data-bs-target="#exampleModal"
                          className="view_cart"
                        >
                          ADD NEW ADDRESS
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div
              className="modal login-form fade"
              id="exampleModal"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="text-end">
                    <button
                      type="button"
                      onClick={() => setAddressStepper(0)}
                      className="btn-close model_close btn-close-white"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    <div className="col-sm-12">
                      <div className="model_logo_div">
                        <img
                          alt=""
                          src="assets/images/newlogo.png"
                          className="model_logo"
                        />
                      </div>
                      <div className="model_signin">
                        {addressStepper == 0 ? (
                          <>
                            <p className="text-center">Check Pincode</p>
                            <form className="register_form">
                              <input
                                className="form-control"
                                type="text"
                                name="line2"
                                placeholder="Pincode"
                                value={pincode}
                                onChange={(e) => setPincode(e.target.value)}
                              />

                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <button
                                  type="button"
                                  className="sign_in"
                                  style={{ width: "100%" }}
                                  onClick={() => handlePincodeCheck()}
                                >
                                  Submit
                                </button>
                              </div>
                            </form>
                          </>
                        ) : (
                          <>
                            <p className="text-center">Add Address</p>
                            <form className="register_form">
                              <input
                                className="form-control"
                                type="text"
                                name="phone"
                                placeholder="Name"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                              />
                              <input
                                className="form-control"
                                type="text"
                                name="number"
                                placeholder="Phone"
                                value={mobile}
                                onChange={(e) => setMobile(e.target.value)}
                              />
                              <input
                                className="form-control"
                                type="text"
                                name="addressline1"
                                placeholder="Address Line 1"
                                value={line1}
                                onChange={(e) => setLine1(e.target.value)}
                              />
                              <input
                                className="form-control"
                                type="text"
                                name="line2"
                                placeholder="Address Line 2"
                                value={line2}
                                onChange={(e) => setLine2(e.target.value)}
                              />
                              <input
                                className="form-control"
                                type="text"
                                name="line2"
                                placeholder="City"
                                value={city}
                                onChange={(e) => setCity(e.target.value)}
                              />

                              <input
                                className="form-control"
                                type="text"
                                name="line2"
                                placeholder="State"
                                value={adstate}
                                onChange={(e) => setAdState(e.target.value)}
                              />

                              <input
                                className="form-control"
                                type="text"
                                name="line2"
                                placeholder="Pincode"
                                value={pincode}
                                onChange={(e) => setPincode(e.target.value)}
                              />

                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <button
                                  className="sign_in"
                                  style={{ width: "100%" }}
                                  onClick={() => handleAdd()}
                                >
                                  Submit
                                </button>
                              </div>
                            </form>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default AccountAddress;
