import $ from "jquery";
import React, { useContext, useEffect, useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { authContext, headerContext } from "../App";
import LogoWhite from "../images/jootelelo2.png";
// import { loginUser, registerUser, sendOtp, setToken } from "../services/user";
import mensBanner from "../images/Men_banner.webp";
import womenBanner from "../images/Women_Banner.webp";
import { getAllCategory } from "../services/Category";
import { getAllGenders } from "../services/Product";
import {
  loginUser,
  registerUser,
  setToken,
  sendOtp,
  forgotPasswordService,
} from "../services/user";

export default function Login() {
  const [isAuthorized, setIsAuthorized] = useContext(authContext);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [showDropdown, setShowDropdown] = useState(false);
  const [dropdownType, setDropdownType] = useState("");
  const [toggleHeader, setToggleHeader] = useContext(headerContext);

  const [toggleModalType, setToggleModalType] = useState(1); //1 for login 2 for register 3 for forgot password

  const [categoryArr, setCategoryArr] = useState([]);
  const [genederArr, setGenederArr] = useState([]);
  const [tempOtp, settempOtp] = useState("");
  const [compareOtp, setCompareOtp] = useState("");

  const sign_in = async (e) => {
    e.preventDefault();

    try {
      if (email == "") {
        alert("Please Enter a valid Email");
      } else if (password == "") {
        alert("Please Enter a password");
      } else {
        let tempCart = localStorage.getItem("jootelelo-cart");
        let obj = {
          email,
          password,
          cartArr: JSON.parse(tempCart),
        };
        let { data: res } = await loginUser(obj);
        if (res) {
          console.log(res);
          setToken(res.token);
          setIsAuthorized(true);
          alert(res.message);
        }
      }
    } catch (err) {
      if (err?.response?.data?.message) {
        console.log(err?.response?.data?.message);
        alert(err?.response?.data?.message);
      } else {
        console.log(err);
        alert(err);
      }
    }
  };
  return (
    <div className="container-fluid" style={{ height: "65vh" }}>
      <div className="row mt-5">
        <div className="col-lg-4 offset-lg-4 loginContainer">
          <h4 className="text-center">Login</h4>
          <form className="sign_in_form">
            <input
              className="form-control"
              type="email"
              name="email"
              placeholder="Email address"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              className="form-control"
              type="password"
              name="password"
              placeholder="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <Link to="#" className="d-block forgot_password">
              Forgot Password?
            </Link>
            <button className="sign_in" onClick={(e) => sign_in(e)}>
              Sign In
            </button>
            <p className="register_now">
              Don't have an account?
              <span className="register_link">Register now</span>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}
