import React from 'react'

export default function Privacy() {
  return (
    <div className='container'>
      <div className="row">
        <div className='col-10 offset-1'>

          <h4 className='text-center mt-4' >Privacy Policy</h4>
          <p className='my-auto mt-4 mb-4'>
            PRIVACY POLICY
            <br />
            Effective date: 30 January, 2021
            <br />


            1. Introduction

            Welcome to  www.Jootelelo.in (“Website”) and Jootelelo Application (“Application”), a platform that brings together student engagement, content delivery and administration tools in one place and is focused on helping tutors go online to support a blended learning approach and digital learning solutions through video conferencing and related services for digital learning solutions (Website and Mobile App are hereinafter collectively referred to as “Platform”) owned and managed by BrandCobblers Services Private Limited (“Company”).

            Your visit and use of platform for student engagement, content delivery, administration tools, digital learning solutions through video conferencing, collaboration, and related services, support, professional services offered on the Platform (“Services”) are subject to this privacy, security and cookies policy ("Privacy Policy") and other terms and conditions of the Platform. This Privacy Policy ensures our firm commitment to your privacy vis-à-vis the protection of your information.

            In this Privacy Policy "we", "our" and "us" refers to Jootelelo and "you", "your" and/or “Users” refers to the user of the Platform. Our Platform is for teachers and students.

            You must be 18 years of age or older to visit or use the Website and/or Application in any manner. If you are under the age of 18, you should review this Privacy Policy with your parent or legal guardian to make sure that you and your parent or legal guardian understands and agrees to it and further, ,if required, you shall perform or undertake such activities which will entitle you to enter into a legally binding agreement with the Company. By visiting this Website and/or Application or
            accepting this Privacy Policy, you represent and warrant to the Company that you are 18 years of age or older, and that you have the right, authority and capacity to use the Website and/or Application and agree to and abide by this Privacy Policy. You also represent and warrant to the Company that you will use this Privacy Policy in a manner consistent with any and all applicable laws and regulations.

            This document is published and shall be construed in accordance with the provisions of the Information Technology (Reasonable Security Practices and Procedures and Sensitive Personal Data of Information) Rules, 2011 (“Data Protection Rules”) under the Information Technology Act, 2000; that require publishing of the Privacy Policy for collection, use, storage and transfer of information. The information collected from you could be categorized as “Personal Information”, “Sensitive
            Personal Information” and “Associated Information”. Personal Information, Sensitive Personal Information and Associated Information (each as individually defined under the Data Protection Rules) shall collectively be referred to as “Information” or “Personal Information” or “Personal Data” in this Policy

            This Privacy Policy is an electronic record in the form of an electronic contract formed under the Information Technology Act, 2000, rules made thereunder, and any other applicable statutes, as amended from time to time. This Privacy Policy does not require any physical, electronic or digital signature.

            Please read this Privacy Policy carefully. By using the Platform, you indicate, agree and acknowledge that you understand, agree and consent to this Privacy Policy and to the collection and use of information in accordance with this Privacy Policy.

            Our Privacy Policy governs your use of the Services on the Platform, and explains how we collect, safeguard and disclose information that results from your use of the Service. We reserve the right to amend and supplement to complete this Privacy Policy at any time. Whenever there is a change to our Privacy Policy, we post updates on our Website and/or Application. We encourage you to regularly review this Privacy Policy to get the latest updates to ensure you know and exercise the right to manage your Personal Information.

            Our Terms and Conditions (“Terms”) govern all use of our Service and together with the Privacy Policy constitutes your agreement with us (“Agreement”).



            2. Definitions

            Unless otherwise defined elsewhere in this Privacy Policy, the following terms shall have the meanings assigned to them as herein below for the purposes of this Privacy Policy:

            “USAGE DATA” means information and data collected and captured as a result of the use of Service.

            “COOKIES” mean small data files stored on your device (computer or mobile device).

            “DATA CONTROLLER” means a natural or legal person who (either alone or jointly or in common with other persons) determines the purposes for which and the manner in which any Personal Data are, or are to be, processed. For the purpose of this Privacy Policy, we are a Data Controller of your data.

            “DATA PROCESSORS” OR “SERVICE PROVIDERS” means any natural or legal person who processes the data on behalf of the Data Controller. We may use the services of various Service Providers in order to process your data more effectively.


            3. Information Collection and Use

            3.1 Collection of Data

            In order to avail the Services under the Platform, registration by the User is mandatory. A valid mobile number would be required to register on the Platform, to complete the registration process, a one time password (OTP) would be sent to you on the mobile number provided by you at the time of registration for the purpose of user validation. You may also be required to generate OTP for downloading any content/material from the Platform, in all cases the OTP will be sent to the mobile number provided at the time of registration. User can also log in through third party service provider under identification mechanism, including verification process through Truecaller verification process. You are fully responsible for all activities that occur under your account. You specifically agree that the Company shall not be responsible for unauthorized access to or alteration of your transmissions or data, any material or data sent or received or not sent or received through the Platform. By registering, you consent to provide information including Personal Data.

            The Company respects the privacy of the Users of the Services and is committed to reasonably protect it in all respects. The information about the user as collected by the Company are:


            Information supplied by Users
            Information automatically tracked while navigation
            Inferred information through usage and log data
            Information collected from any other sources (like third party providers or social media platforms)

            In order to provide Services and to maintain and improve the Platform, we require certain information (including Personal Data) and may obtain your Personal Data when you register with us, when you express an interest in obtaining information about us or our products and Services, when you participate in activities on our Platform or otherwise contact us. Such Personal Data may be collected in various ways including during the course of your:

            Registration as a User on the Platform;
            Availing Services offered on the Platform. Instances of such Services include but are not limited to, communicating with the Company customer service by phone, email or otherwise or posting user reviews in relation to Services/products available on the Platform;
            Granting permission to share credentials of your online accounts maintain with third parties. We may receive Personal information about you from such third parties, including commercially available sources and business partners. If you access the Platform through a connect a service on the Platform, the information we collect may include your information available through such connected service.
            3.2 Types of Data Collected

            We collect different types of information, including Personal Data, for various purposes to provide and improve our Service for you.


            Personal Data
            While using our Service, we may ask you to provide us with certain Personal Data i.e. personally identifiable information that can be used to contact or identify you. Personal Data may include, but is not limited to:

            Email address
            First name and last name
            Phone number
            Address, Country, State, Province, ZIP/Postal/Pin code, City
            Other contact information


            Cookies and Usage Data

            We may use your Personal Data to contact you with newsletters, marketing or promotional materials and other information that may be of interest to you. You may opt out of receiving any, or all, of these communications from us by following the unsubscribe link.



            Usage Data
            We may also collect information that your browser sends whenever you visit our Platform and/or use the Service or when you access the Platform by or through any device (“Usage Data”).

            This Usage Data may include information such as server log, your computer’s Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.

            When you access the Platform with a device, this Usage Data may include information such as the type of device you use, your device unique ID, the IP address of your device, your device operating system, the type of Internet browser you use, unique device identifiers and other diagnostic data.






            Tracking Cookies Data
            We use cookies and similar tracking technologies to track the activity on our Platform and we hold certain information.

            Cookies are files with a small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Other tracking technologies are also used such as beacons, tags and scripts to collect and track information and to improve and analyze our Service.

            You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.

            Examples of Cookies we use:

            Session Cookies: We use Session Cookies to operate our Service.
            Preference Cookies: We use Preference Cookies to remember your preferences and various settings.
            Security Cookies: We use Security Cookies for security purposes.
            Advertising Cookies: Advertising Cookies are used to serve you with advertisements that may be relevant to you and your interests.




            Other Data
            We may also collect the following information and data: place of birth, passport details, citizenship, registration at place of residence and actual address, telephone number (work, mobile), details of documents on education, qualification, professional training, employment agreements, non-disclosure agreements, information on bonuses and compensation, information on marital status, family members, social security (or other taxpayer identification) number, office location, photograph, information regarding your transactions on the Platform, (including purchase history), your financial information such as bank account information or credit card or debit card or other payment instrument details and other data.

            3.3 Purpose for Collection and Use of Data

            All required information is Service dependent and the Company may use the above said user Information to, maintain, protect, and improve the Services (including advertising) and for developing new services. Any Personal Data provided by you will not be considered as sensitive if it is freely available and / or accessible in the public domain like any comments, messages, blogs, scribbles available on social platforms like Facebook, twitter etc

            The Company uses the collected data for various purposes including as follows:

            To create and maintain an online account with the Platform;
            to maintain our Platform and enable you to avail the Services;
            to notify you about changes to our Service;
            to allow you to participate in interactive features of our Service when you choose to do so;
            to provide customer support;
            to analyse information so that we can improve our Platform and Services;
            To notify the necessary information related to the Platform and Services and your online account on the Platform from time to time;
            to monitor your usage of our Platform and Services;
            To maintain records and provide you with an efficient, safe and customized experience while using the Platform;
            to detect, prevent and address technical issues;
            To furnish your information to Service Providers to the extent necessary for delivering the relevant Services;
            Aggregated and individual, anonymized and non-anonymized data may periodically be transmitted to Service Providers to help us improve the Platform and our Services;
            to fulfil any other purpose for which you provide it;
            to verify your identity and prevent fraud or other unauthorized or illegal activity;
            to carry out our obligations and enforce our rights arising from any contracts entered into between you and us, including for billing and collection;
            to provide you with notices about your account and/or subscription, including expiration and renewal notices, email-instructions, etc.;
            To dispatch transaction-related communications such as welcome letters, billing reminders, and purchase confirmations;
            to provide you with news, special offers and general information about other goods, services and events which we offer that are similar to those that you have already purchased or enquired about unless you have opted not to receive such information;
            in any other way we may describe when you provide the information;
            To provide aggregate statistics about Users, traffic patterns, and other related information to reputable third parties, however this information when disclosed will be in an aggregate form and does not contain any of personally identifiable information;
            to comply with applicable laws, rules, and regulations; and
            for any other purpose with your consent.
            You agree that we may use Personal Information about you or share the same with third party service provider to improve our marketing and promotional efforts, to analyse site usage, improve the Platform's content and Service offerings, and customise the Platform's content, layout, and Services. These uses improve the Platform and better tailor it to meet your needs, so as to provide you with an efficient, safe and customized experience while using the Platform. It is clarified that the third-party service provider shall not have right with respect to Personal Information and shall use the Personal Information for the limited purpose for which it has been provided or retain the same for purposes of compliance of applicable law or in complete anonymize form for analytical purposes. Such third-party service provider may store the Personal Information in the servers located in India or outside India.


            3.4 Retention of Data

            We will retain your Personal Data only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use your Personal Data to the extent necessary to comply with our legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve disputes, and enforce our legal agreements and policies.

            We will also retain Usage Data for internal analysis purposes. Usage Data is generally retained for a shorter period, except when this data is used to strengthen the security or to improve the functionality of our Service, or we are legally obligated to retain this data for longer time periods.

            We reserve the right to retain the aggregated and anonymized data derived from the Personal Data collected from the users of the Platform including your Personal Data, for any purposes (commercial and non-commercial) as the Company deem fit and proper.


            4. Disclosure of Data

            We may disclose Personal Data that we collect, or you provided on our Platform as follows:


            4.1 Disclosure for Law Enforcement and other legal necessity.

            We reserve the right to use or disclose your Personal Data in response to any statutory or legal requirements. We will use and disclose your Personal Data if we believe you will harm the property or our rights or right of the other Users of our Platform. Finally, we will use or disclose your Personal Data if we believe it is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person or property, violations of the Platform’s other policies, or as otherwise required by law when responding to subpoenas, court orders and other legal processes.

            We reserve the right to use or disclose your Personal Data and other information if we believe that disclosure is necessary or appropriate to protect rights, property and safety of the Company, its users, customers, Service Providers, officers and shareholders. We may also in good faith share information with other organizations and entities for the purposes of fraud protection and credit risk reduction.


            4.2 Business Transaction

            If we or our subsidiaries are involved in a merger, acquisition or asset sale, and/or in the event the ownership or control of our Platform changes, we may transfer your information to the new owner.


            4.3 Disclosure by the User

            When you use certain features on the Platform like the discussion forums and you post or share your Personal Information as comments, messages, files, photos on such discussion forums on the Platform, which is freely accessible to group of other users and/or the public, the same will be available to all such users and/or public. All such sharing of information will be done at your own risk. Please keep in mind that if you disclose Personal Information in posting as comments, messages, files, photos on the discussion forums on the Platform, such information may become publicly available.


            4.4 Other Disclosures

            We may also disclose your Personal Data as follows:

            to our subsidiaries and affiliates;
            to contractors, Service Providers, and other third parties we use to support our business in compliance with the provisions of this Privacy Policy;
            to fulfill the purpose for which you provide it;
            for the purpose of including your institution / company’s logo on our website;
            for any other purpose disclosed by us when you provide the information; and
            with your consent in any other cases.
            4.5 Data Processing by the Service Providers

            We may engage and employ third party companies and individuals to perform certain processing activities and to perform Service (“Service Providers”), to provide Service on our behalf, perform Service-related activities or assist us in analysing how our Service is used. Towards that objective, we may share your Personal Information with such Service Providers.

            The Service Providers may be engaged and employed for various purposes including following:

            Analytics
            We may use Service Providers to monitor and analyze the use of our Service and the Usage Data.
            CI/CD tools
            We may use Service Providers to update and automate the development process of our Platform.
            Behavioral Remarketing
            We may use remarketing services to advertise on third party websites to you after you visited our Platform. We and our Service Provider use cookies to inform, optimise and serve ads based on your past visits to our Platform.



            5. Data Security Practice and Procedure

            The security of your data is important to us but remember that no method of transmission over the Internet or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.


            5.1 Security of Data

            We maintain strong security safeguards to ensure the security and confidentiality of the User’s data, protect against any anticipated threats to the security of your Personal Information, protect against unauthorized access to your information, and ensure the proper disposal of your Personal Information. We have adopted a comprehensive security policy that complies with laws and regulations and reflects industry standards. We have in place appropriate safeguards and security measures to protect the personal data you provide on this Platform against accidental or any other unlawful forms of processing. We use third party Service Providers for the purposes of payment collections and information shared by you in relation to payments, shall be governed by privacy policy adopted by such Service Providers.

            Our security policy requires us to consistently monitor the risk of any threats to the administrative, technical, and [physical] safeguards we have put in place to protect your information. The safeguards include, at a minimum, firewalls and data encryption and restricting access to your Personal Information only to those employees who have a need to know such information to carry on our operations. These safeguards are reviewed and adjusted periodically based on ongoing risk assessments.


            5.2 Transfer of Data

            The information, including Personal Data, we obtain from or about you may be maintained, processed and stored by us on the systems situated in the territory of Republic of India.

            However, subject to applicable laws, your information, including Personal Data, may be transferred to – and maintained on – computers located in the countries other than India where the data protection laws may differ from those of the Data Protection Rules applicable in India.

            If you are located outside India and choose to provide information to us, please note that we transfer the data, including Personal Data, to India and process it there.

            Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.

            The Company will take all the steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organisation or a country unless there are adequate controls in place including the security of your data and other Personal Information.

            We comply with generally accepted industry standard regulations regarding the collection, use, and retention of data under the Information Technology Act, 2000 and Data Protection Rules. By using the Platform and/or Services, you consent to the collection, transfer, use, storage and disclosure of your information as described in this Privacy Policy, including to the transfer of your information outside of your country of residence.




            9. COPYRIGHT, TRADEMARK, AND OTHER INTELLECTUAL PROPERTY PROTECTION

            Jootelelo is an intellectual property of the Company, all materials on the Platform are protected by copyright laws, trademark laws, and other intellectual property laws. Any unauthorized use of any such information or materials may violate copyright laws, trademarks laws, intellectual property laws, and other laws and regulations.


            10. Contact Us

            If you have any questions about this Privacy Policy, please contact us by email: support@Jootelelo.in.

          </p>
        </div>
      </div>
    </div>
  )
}
