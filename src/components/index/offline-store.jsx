import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getAllStores } from '../../services/store';
import { generateImageUrl } from '../../services/url';


function OfflineStore() {
    const [storeArr, setStoreArr] = useState([]);

    const getStores = async () => {
        try {
            const { data: res } = await getAllStores();
            if (res.success) {
                setStoreArr(res.data)
            }
        } catch (error) {
            console.error(error)
        }
    }



    const handleMapRedirect=(address)=>{
        window.location.href=`http://maps.google.com/?q=${address}`
    }





    useEffect(() => {
        getStores()
    }, [])
    return (
        <section id="offlinestores_contact">
                <div className="container">
                    <h3 className="heading">Offline Stores</h3>
                    <div className="offlinestores_contact_main">
                        <div className="row justify-content-around">
                            {
                                storeArr.map((el, index) => {
                                    return (

                                        <div onClick={()=>handleMapRedirect(el.address)} className="col-sm-4 my-3">
                                            <div className="offlinestore_locations">
                                                <img src={generateImageUrl(el.imageStr)} className="img-fluid" alt='' />
                                                <div className="offlinestore_locations">
                                                    <i className="fas fa-map-pin"></i>
                                                    <div className="offlinestore_locations_text">
                                                        <h3>{el.name}</h3>
                                                        <p>{el.address}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }

                        </div>
                    </div>
                </div>
            </section>
    );
}

export default OfflineStore;