import React from "react";
import { useNavigate } from "react-router-dom";
function ShoppingFor() {
  const navigate = useNavigate();

  const handleRedirect = (obj) => {
    // $(".dropdown-menu").slideToggle();
    // navigate(`/products?gender=${obj.gender}&category=${obj.category}`);
    navigate(`/products?gender=${obj.gender}`);
  };
  return (
    <section id="shoping_for">
      <div className="container">
        <h3 className="heading">Shop Now</h3>
        <div className="row">
          <div className="col-sm-4">
            <div
              className="shoping_for men"
              onClick={() => handleRedirect({ gender: "MEN" })}
            >
              <h4>MEN</h4>
            </div>
          </div>
          <div className="col-sm-4">
            <div
              className="shoping_for women"
              onClick={() => handleRedirect({ gender: "WOMEN" })}
            >
              <h4>WOMEN</h4>
            </div>
          </div>
          <div className="col-sm-4">
            <div
              className="shoping_for kids"
              onClick={() => handleRedirect({ gender: "MEN" })}
            >
              <h4>KIDS</h4>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default ShoppingFor;
