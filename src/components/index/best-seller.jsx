import React, { useState, useEffect, useContext } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { getAllProducts, getProductForUsersWebHomepage } from '../../services/Product';
import { addToWishlist } from '../../services/wishlist';
import ProductImage from '../../images/bestseller_shoe_newfive.webp'
import { loadingContext } from '../../App';

function BestSeller() {
    const [isLoading, setIsLoading] = useContext(loadingContext);

    const [productArr, setProductArr] = useState([])
    const navigate = useNavigate()


    const getAllProductsOnInit = async () => {
        setIsLoading(true)
        try {
            let { data: res } = await getProductForUsersWebHomepage(1, 20)
            if (res.data) {
                console.log(res.data, "@@@@@@@@@@asasddsd@@")
                let tempArr = res.data.map(el => {
                    let obj = {
                        ...el,
                        img1: el.imageArr?.length && el.imageArr[0]?.url ? el.imageArr[0]?.url : ProductImage,
                        img2: el.imageArr?.length && el.imageArr[0]?.url ? el.imageArr[0]?.url : ProductImage,
                    }
                    return obj
                })
                setProductArr([...tempArr])
            }
        }
        catch (err) {

            console.log(err)
        }
        setIsLoading(false)
    }


    const handleSinglePageRedirect = (id) => {
        navigate(`/single-product/${id}`)
    }


    const addItemToWishlist = async (item) => {
        try {
            let obj = {
                productId: item.productId,
                variantId: item.variantId
            }
            const { data: res } = await addToWishlist(obj);
            if (res.success) {
                alert(res.message)
            }
        } catch (error) {
            console.error(error)
        }
    }



    useEffect(() => {

        getAllProductsOnInit()

    }, [])
    return (
        <section id="best_seller">
            <div className="container">
                <h3 className="heading">Best Seller</h3>
                <div className="best_seller_main">
                    {productArr.length > 0 && productArr.map((item, index) => {
                        return (
                            <div style={{ cursor: 'pointer' }} key={index} className="best_seller_container" onClick={() => handleSinglePageRedirect(item.productId)}>
                                <div className="seller_container_main">
                                    <img src={item.img1} className="img-fluid productimage_one" alt='' />
                                    <img src={item.img2} className="img-fluid productimage_two" alt='' />
                                    <div className="new_arrival">
                                        <p>New <br /> Arrival</p>
                                    </div>
                                    {item.discount > 0 &&
                                        <div className="discount">
                                            <p>{item.discount * 100}% Discount</p>
                                        </div>
                                    }
                                    <div className="product_hover_effet">
                                        <div className="hover_icons">
                                            <i onClick={() => handleSinglePageRedirect(item.productId)} className="fas fa-info-circle"></i>
                                            <i onClick={() => addItemToWishlist(item)} className="far fa-heart"></i>
                                        </div>
                                    </div>
                                </div>
                                <div className="product_about">
                                    <h5 className="shoe_name">{item.name}</h5>
                                    <h6 >{item.brandName}</h6>
                                    <h6 >{item.article}</h6>


                                    <div className="price_sec">
                                        <div className="extra">
                                            {item.discount > 0 ?
                                                <div className='mb-2' style={{ display: "flex", flexDirection: "row", alignItems: "center", width: "137%", justifyContent: "space-between" }} >
                                                    <span><s>₹{Math.round(item.price)}</s></span>
                                                    <span>Dis{item.discount * 100}% Off</span>
                                                    <span>₹{Math.round(item.realizedValue)}</span>
                                                </div>
                                                :
                                                <h5>₹{Math.round(item.realizedValue)}</h5>

                                            }
                                            <div className="ratting">
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star"></i>
                                                <i className="fas fa-star-half-alt"></i>
                                            </div>
                                        </div>
                                        {/* <div className="cart_icon">
                                            <i className="fas fa-shopping-cart"></i>
                                        </div> */}
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </section>
    );
}

export default BestSeller;