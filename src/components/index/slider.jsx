import React, { useState, useEffect } from "react";
import { getFirstBanner } from "../../services/Banner";
import { generateImageUrl } from "../../services/url";
import SimpleImageSlider from "react-simple-image-slider";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
function Slider(props) {
  const [bannerArr, setBannerArr] = useState([]);
  console.log(props);
  const navigate = useNavigate();

  const [sliderWidth, setSliderWidth] = useState(0);
  const [articleNoArr, setArticleNoArr] = useState([]);
  const [sliderHeight, setSliderHeight] = useState(0);
  const customStyle = {
    // maxWidth: "100%",
  };

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 1,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  const getBanners = async () => {
    try {
      let { data: res } = await getFirstBanner(props.position);
      if (res.data) {
        let tempWidth = window.innerWidth - 5;
        setSliderWidth(tempWidth);
        let tempHeight = window.innerHeight - 250;
        setSliderHeight(tempHeight);
        // setBannerArr([...res.data.imageArr.map((el, i) => {
        //   let obj = {
        //     img: generateImageUrl(el),
        //     id: i + 1,
        //     animation: i % 2 == 0 ? "animate__bounceInLeft" : "animate__bounceInDown",
        //     active: i == 0,
        //     class: 'slider_one'
        //   }
        //   return obj
        // })])
        setArticleNoArr(res.data.articleNoArr);
        setBannerArr([
          ...res.data.imageArr.map((el) => {
            let obj = {
              url: generateImageUrl(el),
            };
            return obj;
          }),
        ]);
      }
    } catch (err) {
      console.log(err);
    }
  };
  const handleRedirect = (index) => {
    // console.log(e);
    if (articleNoArr[index].productId) {
      navigate(`/single-product/${articleNoArr[index].productId}`);
    }
  };

  useEffect(() => {
    getBanners();
    return () => setBannerArr([]);
  }, []);
  return (
    <section id="banner">
      {bannerArr.length > 0 && (
        // <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
        //   <div className="carousel-inner">
        //     {bannerArr.map((item) => {
        //       return (
        //         <div key={item.id} className={item.active ? "carousel-item active" : "carousel-item"} >
        //           <div className={`slider_main ${item.class}`} style={{ backgroundImage: `url(${item.img})`,backgroundSize:'contain',backgroundPosition:'center',height:'56vw',margin:0}}>
        //             <div className="container">
        //               {/* <h1 className={`animate__animated ${item.animation}`}>{item.h1}</h1>
        //             <p className={`animate__animated ${item.animation}`}>{item.p}</p> */}
        //               <div className={`banner_button animate__animated ${item.animation}`}>
        //                 <Link to="/products">
        //                   <button className="banner_button">Shop Now</button>
        //                 </Link>
        //               </div>
        //             </div>
        //           </div>
        //         </div>
        //       )

        //     })}
        //   </div>
        //   <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        //     <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        //     <span className="visually-hidden">Previous</span>
        //   </button>
        //   <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next" >
        //     <span className="carousel-control-next-icon" aria-hidden="true" ></span>
        //     <span className="visually-hidden">Next</span>
        //   </button>
        // </div>
        <div className="sliderClass">
          {/* <SimpleImageSlider
            onClick={(e) => handleRedirect(e)}
            width={sliderWidth}
            height={sliderWidth < 600 ? 250 : sliderHeight}
            autoPlay={true}
            style={customStyle}
            images={bannerArr}
            // showBullets={true}
            showNavs={true}
          /> */}
          <Carousel
            swipeable={false}
            draggable={false}
            showDots={true}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            // autoPlay={this.props.deviceType !== "mobile" ? true : false}
            autoPlaySpeed={4000}
            keyBoardControl={true}
            customTransition="all .5"
            transitionDuration={500}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            // deviceType={this.props.deviceType}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
          >
            {bannerArr.map((el, index) => {
              return (
                <div onClick={() => handleRedirect(index)}>
                  <img
                    className="img-fluid"
                    style={{
                      height: sliderWidth < 600 ? 250 : sliderHeight,
                      width: sliderWidth,
                    }}
                    src={el.url}
                  />
                </div>
              );
            })}
          </Carousel>
        </div>
      )}
    </section>
  );
}

export default Slider;
