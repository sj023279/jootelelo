import React from "react";
import SupportPic from "../../images/headset.png";
import Delivery from "../../images/cruise.png";
import Price from "../../images/credit-cards-payment.png";
import Return from "../../images/box.png";

export default function Benifits() {
  return (
    <div className="container mt-4 py-4">
      <div className="row ">
        <div className="col-lg-12">
          {/* <div className="row">
            <h3 className="text-center">Our Benifits</h3>
          </div> */}
          <div className="row mainBenifitsContainer">
            <div
              className="col-3 d-flex align-items-center"
              style={{ flexDirection: "column" }}
            >
              <img src={SupportPic} style={{ height: 60, width: 60 }} />
              <h5 className="mt-2">24/7 support</h5>
            </div>

            <div
              className="col-3 d-flex align-items-center"
              style={{ flexDirection: "column" }}
            >
              <img src={Delivery} style={{ height: 60, width: 60 }} />

              <h5 className="mt-2">Delivery with care</h5>
            </div>
            <div
              className="col-3 d-flex align-items-center"
              style={{ flexDirection: "column" }}
            >
              <img src={Price} style={{ height: 60, width: 60 }} />

              <h5 className="mt-2">Economical Pricing</h5>
            </div>
            <div
              className="col-3 d-flex align-items-center"
              style={{ flexDirection: "column" }}
            >
              <img src={Return} style={{ height: 60, width: 60 }} />

              <h5 className="mt-2">Easy Returns</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
