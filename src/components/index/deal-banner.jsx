import React, { useState, useEffect } from "react";
import { getFirstBanner } from "../../services/Banner";
import { generateImageUrl } from "../../services/url";
import SimpleImageSlider from "react-simple-image-slider";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";

function DealBanner() {
    const [bannerArr, setBannerArr] = useState([]);
    const navigate = useNavigate();

    const [sliderWidth, setSliderWidth] = useState(0);
    const [articleNoArr, setArticleNoArr] = useState([]);
    const [sliderHeight, setSliderHeight] = useState(0);
    const customStyle = {
        maxWidth: "100%",
    };

    const getBanners = async () => {
        try {
            let { data: res } = await getFirstBanner("DEALOFTHEDAY");
            if (res.data) {
                let tempWidth = window.innerWidth - 5;
                setSliderWidth(tempWidth);
                console.log(tempWidth);
                let tempHeight = window.innerHeight + 50;
                setSliderHeight(tempHeight);
                console.log(res.data, "@@@@@@@@@@asasddsd@@");

                setArticleNoArr(res.data.articleNoArr)
                setBannerArr([
                    ...res.data.imageArr.map((el) => {
                        let obj = {

                            url: generateImageUrl(el),

                        };
                        return obj;
                    }),
                ]);
            }
        } catch (err) {
            console.log(err);
        }
    };
    useEffect(() => {
        getBanners()
    }, [])
    return (
        <>
            {bannerArr.length > 0 ?

                <section id="dealsof_day" style={{ backgroundImage: `url(${bannerArr[0]?.url})` }}>
                    <div className="container">
                        <img src="assets/images/shoe_transparent.png" className="transparent_shoe" alt='' />
                        <h3 className="dealsof_day_text">Deals Of <span className="dealsof_day_text_span"> The Day </span></h3>
                        <h1 className="hurry_now">HURRY</h1>
                        {/* <Link to="/products">
                    <button className="banner_button">Claim Now</button>
                </Link> */}
                        <p className="before_deals_end">BEFORE OFFER ENDS</p>
                    </div>
                </section>
                :
                <section id="dealsof_day" style={{ backgroundImage: `url(${bannerArr[0]?.url})` }}>
                    <div className="container">
                        <img src="assets/images/shoe_transparent.png" className="transparent_shoe" alt='' />
                        <h3 className="dealsof_day_text">Deals Of <span className="dealsof_day_text_span"> The Day </span></h3>
                        <h1 className="hurry_now">HURRY</h1>
                        {/* <Link to="/products">
                <button className="banner_button">Claim Now</button>
            </Link> */}
                        <p className="before_deals_end">BEFORE OFFER ENDS</p>
                    </div>
                </section>
            }
        </>
    );
}

export default DealBanner;