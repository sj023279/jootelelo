import React from "react";
import bgImage from "../../images/StoreImage2(1).jpg";
export default function BannerStatic() {
  return (
    // <div style={{ position: "relative" }}>
    //   <img
    //     src={bgImage}
    //     style={{
    //       height: 480,
    //       width: "90vw",
    //       marginLeft: "5vw",
    //       borderRadius: 15,
    //     }}
    //     alt=""
    //   />
    //   <div
    //     style={{
    //       position: "absolute",
    //       top: "140px",
    //       fontSize: "22px",
    //       color: "white",
    //       left: "50%",
    //       transform: "translateX(-50%)",
    //     }}
    //   >
    //     New Arrivals
    //   </div>
    //   <div
    //     style={{
    //       position: "absolute",
    //       top: "170px",
    //       fontSize: "38px",
    //       color: "white",
    //       left: "50%",
    //       transform: "translateX(-50%)",
    //     }}
    //   >
    //     Mens Shoes
    //   </div>
    //   <div
    //     style={{
    //       position: "absolute",
    //       top: "230px",
    //       fontSize: "17px",
    //       color: "white",
    //       left: "50%",
    //       transform: "translateX(-50%)",
    //     }}
    //   >
    //     We've handpicked these for you to have a look !
    //   </div>
    //   <div
    //     style={{
    //       position: "absolute",
    //       bottom: "100px",
    //       fontSize: "17px",
    //       color: "white",
    //       border: "solid 2px white",
    //       padding: "10px 25px",
    //       backgroundColor: "rgba(255,255,255,0.2)",
    //       borderRadius: 5,
    //       left: "50%",
    //       transform: "translateX(-50%)",
    //     }}
    //   >
    //     Shop now
    //   </div>
    // </div>
    <section id="dealsof_day_static" style={{ backgroundImage: bgImage }}>
      <div className="container">
        {/* <img
          src="assets/images/shoe_transparent.png"
          className="transparent_shoe"
          alt=""
        /> */}
        <h3 className="dealsof_day_text">
          <span className="dealsof_day_text_span"> New Arrival </span>
        </h3>
        <h1 className="hurry_now">Men Shoes</h1>
        {/* <Link to="/products">
                <button className="banner_button">Claim Now</button>
            </Link> */}
        <p className="before_deals_end">
          {" "}
          We've handpicked these for you to have a look !
        </p>
      </div>
    </section>
  );
}
