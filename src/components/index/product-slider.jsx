import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Autoplay, Navigation } from "swiper";
import "swiper/css";
import "swiper/css/autoplay";
import "swiper/css/navigation";
import { Swiper, SwiperSlide } from "swiper/react";
import { authContext, headerContext, loadingContext } from "../../App";
import ProductImage from "../../images/bestseller_shoe_newfive.webp";
import { getAllProducts } from "../../services/Product";
import { addToWishlist } from "../../services/wishlist";

function ProductSlider({ heading }) {
  const [productArr, setProductArr] = useState([]);

  const navigate = useNavigate();
  const [isAuthorized, setIsAuthorized] = useContext(authContext);
  const [toggleHeader, setToggleHeader] = useContext(headerContext);
  const [isLoading, setIsLoading] = useContext(loadingContext);
  const prevRef = React.useRef();
  const nextRef = React.useRef();

  const responsive = {
    0: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    576: {
      slidesPerView: 1,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
  };

  const getAllProductsOnInit = async () => {
    // setIsLoading(true)
    try {
      let { data: res } = await getAllProducts(1, 50);
      if (res.data) {
        let tempArr = res.data
          .filter((el) => el.imageArr && el.imageArr.length > 0)
          .map((el) => {
            let obj = {
              ...el,
              img1:
                el.imageArr?.length && el.imageArr[0]?.url
                  ? el.imageArr[0]?.url
                  : ProductImage,
              img2:
                el.imageArr?.length && el.imageArr[0]?.url
                  ? el.imageArr[0]?.url
                  : ProductImage,
            };
            return obj;
          });
        if (heading == "New Arrivals") {
          tempArr = tempArr.reverse();
        }
        if (heading == "Men's Collection") {
          tempArr = tempArr.filter((el) => el.gender == "MEN").reverse();
        }
        if (heading == "Women's Collection") {
          tempArr = tempArr.filter((el) => el.gender == "WOMEN").reverse();
        }
        setProductArr([...tempArr]);
      }
    } catch (err) {
      console.log(err);
    }
    // setIsLoading(false)
  };

  const addItemToWishlist = async (item) => {
    setIsLoading(true);
    try {
      if (isAuthorized) {
        let obj = {
          productId: item.productId,
          variantId: item.variantId,
        };
        const { data: res } = await addToWishlist(obj);
        if (res.success) {
          alert(res.message);
        }
      } else {
        console.log("unauthorized");
        setToggleHeader(true);
      }
    } catch (error) {
      console.error(error);
    }
    setIsLoading(false);
  };

  const handleSinglePageRedirect = (id) => {
    navigate(`/single-product/${id}`);
  };
  const handleRedirect = () => {
    navigate(`/products`);
  };

  useEffect(() => {
    getAllProductsOnInit();

    return () => setProductArr([]);
  }, []);

  return (
    <section id="shop_now_slider">
      <div className="container ">
        <div className="row  mb-2">
          <div className="col-12">
            <div className="row">
              <div className="col-4">
                <h3 className="headingMain">{heading}</h3>
              </div>
              <div className="col-6">
                {/* <div className="bottomLine"></div> */}
              </div>
              <div className="col-2  d-flex  align-items-center justify-content-center ">
                <i
                  class="fa-solid fa-chevron-left"
                  ref={prevRef}
                  style={{ fontSize: 30, marginRight: 5, cursor: "pointer" }}
                ></i>
                <i
                  class="fa-solid fa-chevron-right ms-3"
                  ref={nextRef}
                  style={{ fontSize: 30, cursor: "pointer" }}
                ></i>
              </div>
            </div>
          </div>
          {/* <div className="d-flex align-items-center justify-content-center">
            <div className="bottomLine"></div>
          </div> */}
        </div>
        {productArr.length > 0 && (
          <Swiper
            spaceBetween={500}
            modules={[Autoplay, Navigation]}
            allowSlidePrev={true}
            breakpoints={responsive}
            autoplay={true}
            zoom={true}
            navigation={{
              prevEl: prevRef?.current,
              nextEl: nextRef?.current,
            }}
            // navigation={true}
            // onSlideChange={() => console.log('slide change')}
            // onSwiper={(swiper) => console.log(swiper)}
          >
            {productArr.map((item, index) => {
              return (
                <SwiperSlide key={index}>
                  <div
                    className="item"
                    onClick={() => handleSinglePageRedirect(item.productId)}
                    style={{ cursor: "pointer" }}
                  >
                    <div className="best_seller_container">
                      <div
                        className="seller_container_main"
                        style={{ maxHeight: 300, minHeight: 300 }}
                      >
                        <img src={item.img1} className="img-fluid " alt="" />
                        {item.discount > 0 && (
                          <div className="new_arrival">
                            <p>
                              {Math.round(item.discount * 100)}% <br />
                              Off
                            </p>
                          </div>
                        )}
                      </div>
                      <div className="product_about">
                        <h6>{item.brandName}</h6>
                        <h6>{item.name}</h6>

                        <div className="extra">
                          {item.discount > 0 ? (
                            <div
                              className="mb-2"
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                width: "137%",
                                justifyContent: "space-between",
                              }}
                            >
                              <div className="col-6">
                                <span>
                                  <s>₹ {Math.round(item.price)}</s>
                                </span>
                              </div>
                              <div className="col-6">
                                <span>₹ {Math.round(item.realizedValue)}</span>
                              </div>
                            </div>
                          ) : (
                            <div
                              className="mb-2"
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                width: "137%",
                                justifyContent: "space-between",
                              }}
                            >
                              <div className="col-6">
                                <span>₹{Math.round(item.price)}</span>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>
        )}
      </div>
    </section>
  );
}

export default ProductSlider;
