import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function CartItem({ img, category, product, price, quantity, deleteCart }) {

    let [value, setvalue] = useState(quantity);

 
    const addQuantity = (e) => {
        e.preventDefault();
        value += 1;
        setvalue(value);
    }

    const deleteQuantity = (e) => {
        e.preventDefault();
        if (value >= 1) {
            value -= 1;
            setvalue(value);
        }
    }

    


    return (
        <div className="cart-item border-bottom">
            <div className="row main align-items-center">
                <div className="col-5 d-flex align-items-center">
                    <div className="cart-item-img overflow-hidden">
                        <img className="img-fluid" src={img} alt='' />
                    </div>
                    <div className="cart-content">
                        <div className="text-muted">{category}</div>
                        <div>{product}</div>
                    </div>
                </div>
                <div className="col-3">
                    <div className="add_input">
                        <Link to="#" className='border-end-0' onClick={(e) => deleteQuantity(e)}>-</Link>
                        <Link to="#" className="border-end-0">{value}</Link>
                        <Link to="#" onClick={(e) => addQuantity(e)}>+</Link>
                    </div>
                </div>
                <div className="col-3">
                    <div className="cart-price">
                        <p className='m-0'>&euro; {price}</p>
                    </div>
                </div>
                <div className="col-1">
                    <span className="close" onClick={deleteCart}>&#10005;</span>
                </div>
            </div>
        </div>
    );
}

export default CartItem;