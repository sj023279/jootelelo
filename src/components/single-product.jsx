import React from "react";
import ProductSlider from "./index/product-slider";
import PageBanner from "./page-banner";
import ProductDetail from "./single-product/product-detail";
import ProductShowcase from "./single-product/product-showcase";
import SimilarProducts from "./single-product/similar-products";

function SingleProduct() {
  return (
    <>
      <ProductShowcase />
      {/* <ProductSlider /> */}
      {/* <SimilarProducts/> */}
    </>
  );
}

export default SingleProduct;
