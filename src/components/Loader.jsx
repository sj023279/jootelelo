import React from 'react'
import Lottie from 'react-lottie';
import * as animationData from '../assets/loader.json'
export default function Loader() {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    return (
        <div style={{ height: '100vh', width: '100vw', backgroundColor: 'rgba(0,0,0,0.7)', position: 'fixed', top: 0, left: 0, zIndex: 2000, display: 'grid', placeItems: 'center' }}>
            <Lottie options={defaultOptions}
                height={400}
                width={400}
            // isStopped={this.state.isStopped}
            />
            {/* <button style={buttonStyle} onClick={() => this.setState({ isStopped: true })}>stop</button>
            <button style={buttonStyle} onClick={() => this.setState({ isStopped: false })}>play</button>
            <button style={buttonStyle} onClick={() => this.setState({ isPaused: !this.state.isPaused })}>pause</button> */}
        </div>
    )
}
