import React from 'react'
import ComingSoonImage from '../images/cominsoonlandscape.svg'
import ComingSoonImage2 from '../images/ComingSoon.svg'

export default function Comingsoon() {
  let tempWidth = window.innerWidth
  return (
    <>
      {tempWidth >= 768 ?
        <img src={ComingSoonImage} alt="React Logo" style={{ width: '100vw', height: '100vh', overflowX: 'hidden' }} />

        :

        <img src={ComingSoonImage2} alt="React Logo" style={{ width: '100vw', height: '100vh', overflowX: 'hidden' }} />
      }
    </>
  )
}
