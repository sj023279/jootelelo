import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { getAllStores } from "../services/store";
import { generateImageUrl } from "../services/url";
import PageBanner from "./page-banner";

function Contact() {
  const [storeArr, setStoreArr] = useState([]);

  const getStores = async () => {
    try {
      const { data: res } = await getAllStores();
      if (res.success) {
        setStoreArr(res.data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleMail = () => {
    window.open("mailto:support@gmail.com");
  };
  const handlePhone = () => {
    window.open("tel:9873439378");
  };
  useEffect(() => {
    getStores();
  }, []);

  return (
    <>
      <PageBanner name="Contact Us" />

      <section id="contact_firstsec">
        <div className="container">
          <div className="contact_firstdiv">
            <h3 className="heading">Map Location</h3>

            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14010.10896557153!2d77.0841541!3d28.6139562!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xedd1970c30a49e76!2sAhead%20Retail%20Private%20Limited!5e0!3m2!1sen!2sin!4v1647070706622!5m2!1sen!2sin"
              width="100%"
              height="500"
              id="gmap_canvas"
              allowfullscreen=""
              loading="lazy"
            ></iframe>
          </div>
        </div>
      </section>

      <section id="contact_form">
        <div className="container">
          <h3 className="heading">Contact Us</h3>
          <div className="contact_formdiv">
            <div className="row">
              <div className="col-sm-6">
                <div className="contact_info">
                  <h5 className="headoffice text-capitalize">Head office</h5>
                  <div className="contact_info_main">
                    <i className="fas fa-map-marker-alt"></i>
                    <p>
                      {" "}
                      <Link to="#">
                        RZ-3A Sitapuri, New Delhi-110045. Near Dabri Metro
                        Station
                      </Link>
                    </p>
                  </div>
                  <div className="contact_info_main">
                    <i className="fas fa-phone"></i>
                    <p onClick={() => handlePhone()}>
                      {" "}
                      <span to="tel:9873439378"> +91 9873439378 </span>
                    </p>
                  </div>
                  <div className="contact_info_main">
                    <i className="fas fa-envelope"></i>
                    <p onClick={() => handleMail()}>
                      <span>support@jootelelo.in</span>
                    </p>
                  </div>
                  <div className="social_icon_contact">
                    <i className="fab fa-facebook"></i>
                    <i className="fab fa-twitter"></i>
                    <i className="fab fa-youtube"></i>
                    <i className="fab fa-instagram"></i>
                  </div>
                </div>
              </div>
              <div className="col-sm-6">
                <div className="contact_from1">
                  <form>
                    <input
                      type="text"
                      name="name"
                      placeholder="Name"
                      required
                    />
                    <input
                      type="email"
                      name="email"
                      placeholder="E-mail"
                      required
                    />
                    <input
                      type="tel"
                      name="phone no"
                      placeholder="phone no"
                      required
                    />
                    <textarea
                      name="comment"
                      placeholder="comment"
                      rows="5"
                      className="contact_comment"
                    ></textarea>
                    <input
                      type="submit"
                      onClick={() => alert("We will get back to you soon")}
                      value="send message"
                      className="send_message"
                    />
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="offlinestores_contact">
        <div className="container">
          <h3 className="heading">Offline Stores</h3>
          <div className="offlinestores_contact_main">
            <div className="row justify-content-around">
              {storeArr.map((el, index) => {
                return (
                  <div className="col-sm-4 my-3">
                    <div className="offlinestore_locations">
                      <img
                        src={generateImageUrl(el.imageStr)}
                        className="img-fluid"
                        alt=""
                      />

                      <div className="offlinestore_locations">
                        <i className="fas fa-map-pin"></i>
                        <div className="offlinestore_locations_text">
                          <h3>{el.name}</h3>
                          <p>{el.address}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Contact;
