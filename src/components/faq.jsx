import React from 'react';
import { Link } from 'react-router-dom';
import PageBanner from './page-banner';

function Faq() {
    return (
        <>
            <PageBanner name='FAQ' />

            <section id="faq_first_sec">
                <div className="container">
                    {/* <h2 className="headingnew">Faq's</h2> */}
                    <div className="faq_first_div">
                        <div className="row align-items-center">
                            <div className="col-lg-6">
                                <div className="accordion" id="accordionExample1">
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingOne">
                                            <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                What happens if my payment fails?
                                            </button>
                                        </h2>
                                        <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample1">
                                            <div className="accordion-body">
                                                <strong>In case of payment failure, please retry using the same or different payment option: When re-trying using the same payment option, please check your account details for any errors.For any clarifications and help, you can also get in touch with us via email on support@jootelelo.in or Call : +91 9873439378.
                                                </strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingTwo">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                How do I pay?
                                            </button>
                                        </h2>
                                        <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample1">
                                            <div className="accordion-body">
                                                <strong>The payment options we support are: Credit Card, Debit Card, Net Banking, Cash on Delivery (at selected locations across India)

                                                    Jootelelo.in supports following online payment options through Razor Pay:- Supported Credit Cards, American Express, Diners Club International, JCB Cards, MasterCard, Visa

                                                    Supported Debit Cards:-

                                                    Visa Debit Card issued in India (VBV)

                                                    For Supported Netbanking & other payment options, please refer Razor Pay website</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingThree">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                What is the estimated delivery time?
                                            </button>
                                        </h2>
                                        <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample1">
                                            <div className="accordion-body">
                                                Jootelelo shoes generally ship the items within the time specified below:-
                                                Region Estimated Delivery Time
                                                NCR Delhi 2 to 3 Business Days
                                                North Region 2 to 5 Business Days
                                                Rest of India 2 to 6 Business Days
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingThree">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseseven" aria-expanded="false" aria-controls="collapseThree">
                                                Why is the CoD option not offered in my location?
                                            </button>
                                        </h2>
                                        <div id="collapseseven" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample1">
                                            <div className="accordion-body">
                                                Availability of CoD depends on the ability of our courier partner servicing your location to accept cash as payment at the time of delivery.
                                                Our courier partners have limits on the cash amount payable on delivery depending on the destination and your order value might have exceeded this limit. Please enter your pin code on the product page to check if CoD is available in your location.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingOne">
                                            <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefour" aria-expanded="true" aria-controls="collapseOne">
                                                What do the different tags like "In Stock", "Available" mean?
                                            </button>
                                        </h2>
                                        <div id="collapsefour" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample2">
                                            <div className="accordion-body">
                                                In Stock: For items listed as "In Stock" are available for shipment immediately.

                                                Out of Stock: Currently, the item is not available for sale. Use the 'Notify Me' feature to know once it is available for purchase
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingTwo">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefive" aria-expanded="false" aria-controls="collapseTwo">
                                                Seller does not/cannot ship to my area. Why?
                                            </button>
                                        </h2>
                                        <div id="collapsefive" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample2">
                                            <div className="accordion-body">
                                                Please enter your pincode on the product page (you don't have to enter it every single time) to know whether the product can be delivered to your location.

                                                If you haven't provided your pincode until the checkout stage, the pincode in your shipping address will be used to check for serviceability.

                                                Whether your location can be serviced or not depends on

                                                Whether the Seller ships to your location

                                                Legal restrictions, if any, in shipping particular products to your location

                                                The availability of reliable courier partners in your location
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 text-center">
                                <img width={'100%'} alt='' src="assets/images/question_two.png" />
                            </div>
                            <div className="col-sm-6 text-center">
                                <img alt='' src="assets/images/question_one.png" className="question_one" />
                            </div>
                            <div className="col-sm-6">
                                <div className="accordion" id="accordionExample2">
                                    
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingThree">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsesix" aria-expanded="false" aria-controls="collapseThree">
                                                Are there any hidden costs (sales tax, octroietc) on items sold by Jootelelo?
                                            </button>
                                        </h2>
                                        <div id="collapsesix" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample2">
                                            <div className="accordion-body">
                                                There are NO hidden charges when you make a purchase on Jootelelo. List prices are final and all-inclusive. The price you see on the product page is exactly what you would pay.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingThree">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight1" aria-expanded="false" aria-controls="collapseThree">
                                                Does Jootelelo deliver internationally?
                                            </button>
                                        </h2>
                                        <div id="collapseeight1" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample2">
                                            <div className="accordion-body">
                                                As of now, Jootelelo doesn't deliver items internationally. You will be able to make your purchases on our site from anywhere in the world with credit/debit cards issued in India, but please ensure the delivery address is in India.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingThree">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight2" aria-expanded="false" aria-controls="collapseThree">
                                                What is the exchange process? Why I have been asked to ship the item?
                                            </button>
                                        </h2>
                                        <div id="collapseeight2" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample2">
                                            <div className="accordion-body">
                                                Once you have requested for an exchange, we will schedule the pickup of the originally delivered product. Please ensure that product is in unused and original condition. Include all price tags, labels, original packing and invoice along with the product. Alternately, you can ship it back to us. We will add a fixed shipping charges of Rs.50/= in the credit note. There is NO cash refund. Please note down the courier tracking id for any future reference.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingThree">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight3" aria-expanded="false" aria-controls="collapseThree">
                                                I have requested an exchange, when will I get it?
                                            </button>
                                        </h2>
                                        <div id="collapseeight3" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample2">
                                            <div className="accordion-body">
                                                Once an exchange request is created, we send you an email detailing the pickup process as well as provide details about the exchange. The exchange item is delivered to you at the time of pick-up, wherever possible. In all other areas, the exchange is initiated after the originally delivered item is picked up/received.                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="accordion-item faq_text">
                                    <h2 className="accordion-header" id="headingThree">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight8" aria-expanded="false" aria-controls="collapseThree">
                                            Do I have to return the free gift when I return a product?
                                        </button>
                                    </h2>
                                    <div id="collapseeight8" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample2">
                                        <div className="accordion-body">
                                            Yes. The free gift is included as part of the item order and needs to be returned along with the originally delivered product.
                                        </div>


                                        <div className="accordion-item faq_text">
                                            <h2 className="accordion-header" id="headingThree">
                                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight4" aria-expanded="false" aria-controls="collapseThree">
                                                    What items can I Exchange?
                                                </button>
                                            </h2>
                                            <div id="collapseeight4" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample2">
                                                <div className="accordion-body">
                                                    We allow exchange on items available on our website. In case the item of your choice is not available, a credit note for the full value will be issued.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingThree">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight5" aria-expanded="false" aria-controls="collapseThree">
                                                Can I return part of my order?
                                            </button>
                                        </h2>
                                        <div id="collapseeight5" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample2">
                                            <div className="accordion-body">
                                               No .
                                            </div>
                                        </div>
                                    </div>
                                    <div className="accordion-item faq_text">
                                        <h2 className="accordion-header" id="headingThree">
                                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight6" aria-expanded="false" aria-controls="collapseThree">
                                                Contact Information?
                                            </button>
                                        </h2>
                                        <div id="collapseeight6" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample2">
                                            <div className="accordion-body">
                                                For any other clarifications or information that you may require, kindly contact us at Call : +91 9873439378 or email us at. support@jootelelo.in.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default Faq;