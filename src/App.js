import React, {
  createContext,
  useContext,
  useState,
  useEffect,
  useMemo,
} from "react";
import Header from "./components/header";
import Footer from "./components/footer";
import Index from "./components/index";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Products from "./components/products";
import SingleProduct from "./components/single-product";
import About from "./components/about";
import Contact from "./components/contact";
import Cart from "./components/cart";
import Faq from "./components/faq";
import AccountProfile from "./components/account/account-profile";
import AccountOrder from "./components/account/account-order";
import AccountWishlist from "./components/account/account-wishlist";
import AccountReview from "./components/account/account-review";
import AccountAddress from "./components/account/account-address";
import ViewOrder from "./components/view-order";

import axios from "axios";
import { url } from "./services/url";
import Loader from "./components/Loader";
import Privacy from "./components/Privacy";
import Comingsoon from "./components/Comingsoon";
import ScrollToTop from "./components/ScrollToTop";
import Login from "./components/Login";

export const axiosApiInstance = axios.create();
export const authContext = createContext();
export const headerContext = createContext();
export const loadingContext = createContext();

function App() {
  const [isAuthorized, setIsAuthorized] = useState(false);
  const [toggleHeader, setToggleHeader] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const checkAuthorized = async () => {
    let token = localStorage.getItem("TOKEN");
    if (token) {
      setIsAuthorized(true);
    } else {
      setIsAuthorized(false);
    }
  };

  useMemo(() => {
    console.log("Setting Interceptor");
    axiosApiInstance.interceptors.request.use(
      (config) => {
        const token = localStorage.getItem("TOKEN");
        if (token) {
          config.headers["Authorization"] = "Bearer " + token;
        }
        // config.headers['Content-Type'] = 'application/json';
        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );

    axiosApiInstance.interceptors.response.use(
      (res) => {
        // Add configurations here
        return res;
      },
      async (err) => {
        const originalConfig = err.config;

        if (originalConfig.url !== `${url}/users/login` && err.response) {
          // Access Token was expired
          if (err.response.status === 401 && !originalConfig._retry) {
            // let authToken = localStorage.getItem('AUTH_TOKEN')
            // localStorage.removeItem('AUTH_TOKEN')
            originalConfig._retry = true;

            localStorage.removeItem("TOKEN");
            setIsAuthorized(false);
            alert("Please Login to continue");
          }
        }

        return Promise.reject(err);
      }
    );
  }, []);

  useEffect(() => {
    checkAuthorized();
  }, []);

  return (
    <loadingContext.Provider value={[isLoading, setIsLoading]}>
      <authContext.Provider value={[isAuthorized, setIsAuthorized]}>
        <headerContext.Provider value={[toggleHeader, setToggleHeader]}>
          {/* <Comingsoon/> */}
          <Router>
            <ScrollToTop>
              <Header />
              {isLoading && <Loader />}
              <Routes>
                <Route exact path="/" element={<Index />} />
                <Route path="/products" element={<Products />} />
                <Route path="/single-product/:id" element={<SingleProduct />} />
                <Route path="/about" element={<About />} />
                <Route path="/contact" element={<Contact />} />
                <Route exact path="/account" element={<AccountProfile />} />
                <Route path="/account/order" element={<AccountOrder />} />
                <Route path="/account/wishlist" element={<AccountWishlist />} />
                <Route path="/account/review" element={<AccountReview />} />
                <Route path="/account/address" element={<AccountAddress />} />
                <Route path="/cart" element={<Cart />} />
                <Route path="/faq" element={<Faq />} />
                <Route path="/view-order/:id" element={<ViewOrder />} />
                <Route path="/privacy" element={<Privacy />} />
                {/* <Route path="/login" element={<Login />} /> */}
              </Routes>
              <Footer />
            </ScrollToTop>
          </Router>
        </headerContext.Provider>
      </authContext.Provider>
    </loadingContext.Provider>
  );
}

export default App;
