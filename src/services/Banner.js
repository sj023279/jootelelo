import axios from "axios";
import { url } from './url';

const serverUrl = `${url}/banner`;

export const getAllBanners=async()=>{
    return await axios.get(`${serverUrl}/`)
}

export const getFirstBanner=async(position)=>{
    return await axios.get(`${serverUrl}/getFirstBanner/${position}`)
}
