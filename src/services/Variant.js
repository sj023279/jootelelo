import axios from "axios";
import { url } from './url';

const serverUrl = `${url}/variant`;

export const getVariantForFilter = async () => {
    let res = await axios.get(`${serverUrl}/getVariantForFilter`)
    return res
}

