import axios from "axios";
import { url } from './url';

const serverUrl = `${url}/category`;


export const getAllCategory = async () => {
    return await axios.get(`${serverUrl}/getAllForUsers`)

}