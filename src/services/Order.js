import axios from "axios";
import { url } from './url';
import { axiosApiInstance } from "../App";
import { getDecodedToken } from './user'
const serverUrl = `${url}/order`;



export const createOrder = async (obj) => {
    return await axiosApiInstance.post(`${serverUrl}/createOrder`, obj)
}

export const orderCallback = async (obj, id) => {
    return axiosApiInstance.get(`${serverUrl}/paymentCB/${id}?${obj}`)

}

export const getAllOrders = async () => {
    let tokenObj = await getDecodedToken();
    return axiosApiInstance.get(`${serverUrl}/getByUserId/${tokenObj.userId}`)
}

export const getOrderById = async (id) => {
    return axiosApiInstance.get(`${serverUrl}/getById/${id}`)
}

export const createWalletOrder = async (obj) => {
    return await axiosApiInstance.post(`${serverUrl}/createOrderPayFromWallet`, obj)
}
export const createCodOrder = async (obj) => {
    return await axiosApiInstance.post(`${serverUrl}/createOrderCOD`, obj)
}

export const cancelOrderById = async (id) => {
    return await axiosApiInstance.patch(`${serverUrl}/cancelOrderById/${id}`)
}
