import axios from "axios";
import { url } from './url';
import { axiosApiInstance } from "../App";
const serverUrl = `${url}/store`;



export const getAllStores=async()=>{
    return axios.get(`${serverUrl}/`)
}