import axios from 'axios';
import { axiosApiInstance } from '../App';
import { url } from './url';

const serverUrl = `${url}/address`;

export const getAddressesForUser = async () => {
    return await axiosApiInstance.get(`${serverUrl}/getAddressesForUser`)
}

export const getByAddressId = async (id) => {
    return await axiosApiInstance.get(`${serverUrl}/getById/${id}`)
}
export const updateAddressById = async (id, obj) => {
    return await axiosApiInstance.patch(`${serverUrl}/updateById/${id}`, obj)
}
export const addAddress = async (obj) => {
    return await axiosApiInstance.post(`${serverUrl}/`, obj)
}

export const checkPincodeArea = async (pincode) => {
    return await axiosApiInstance.get(`${serverUrl}/checkPincode/${pincode}`)
}