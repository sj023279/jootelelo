import { axiosApiInstance } from '../App';
import { url } from './url';

const serverUrl = `${url}/wishlist`;


export const addToWishlist = async (obj) => {
    return await axiosApiInstance.post(`${serverUrl}/addToWishList`,obj)
}

export const getAllWishlist=async()=>{
    return await axiosApiInstance.get(`${serverUrl}/`)
}