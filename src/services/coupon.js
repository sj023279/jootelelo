import axios from "axios";
import { url } from "./url";

const serverUrl = `${url}/coupon`;

export const getAllCoupons = async () => {
  return await axios.get(`${serverUrl}/getValidCoupons`);
};
