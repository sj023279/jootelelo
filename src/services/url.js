// export const url = "http://192.168.0.165:4010"
// /export const url = "http://localhost:4010";

export const url = "http://ec2-65-1-55-43.ap-south-1.compute.amazonaws.com";

export const generateImageUrl = (image) => {
  return `${url}/uploads/${image}`;
};
