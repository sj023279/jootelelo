import axios from "axios";
import { url } from './url';

const serverUrl = `${url}/product`;

export const getAllProducts = async (pageCount, itemsPerPage) => {
        let res = await axios.get(`${serverUrl}/getProductForUsersHomepage?currentPage=${pageCount}&itemsPerPage=${itemsPerPage}`)
        return res
}

export const getProductById = async (id) => {
        return await axios.get(`${serverUrl}/getById/${id}`)
}


export const getProductForUsersWebHomepage = async (pageCount, itemsPerPage) => {
        let res = await axios.get(`${serverUrl}/getProductForUsersWebHomepage?currentPage=${pageCount}&itemsPerPage=${itemsPerPage}`)
        return res
}


export const getNewArrivals = async (pageCount, itemsPerPage) => {
        let res = await axios.get(`${serverUrl}/getNewArrivals?currentPage=${pageCount}&itemsPerPage=${itemsPerPage}`)
        return res
}


export const getAllGenders = async () => {
        let res = await axios.get(`${serverUrl}/getAllGenders`)
        return res
}


export const getAllDiscount = async () => {
        let res = await axios.get(`${serverUrl}/getAllDiscount`)
        return res
}



export const getSimilarProducts = async (id) => {
        let res = await axios.get(`${serverUrl}/getSimilarProducts/${id}`)
        return res
}

