import axios from "axios";
import { url } from "./url";
import { axiosApiInstance } from "../App";
import jwt_decode from "jwt-decode";
const serverUrl = `${url}/users`;

export const registerUser = async (obj) => {
  let res = await axios.post(`${serverUrl}/register`, obj);
  return res;
};

export const loginUser = async (obj) => {
  let res = await axios.post(`${serverUrl}/login`, obj);
  return res;
};

export const getUserById = async () => {
  let tokenObj = await getDecodedToken();
  console.log(tokenObj);
  return await axios.get(`${serverUrl}/getById/${tokenObj.userId}`);
};

////////token
export const setToken = async (TOKEN) => {
  localStorage.setItem("TOKEN", TOKEN);
};
export const getDecodedToken = async () => {
  let token = localStorage.getItem("TOKEN");
  let decoded = jwt_decode(token);
  return decoded;
};
export const removeToken = async () => {
  localStorage.setItem("TOKEN");
};
export const getToken = async () => {
  localStorage.setItem("TOKEN");
};

export const addItemToCart = async (obj) => {
  return await axiosApiInstance.patch(`${serverUrl}/addItemsToCart`, obj);
};

export const getUserCart = async () => {
  console.log("getting cart");
  return await axiosApiInstance.get(`${serverUrl}/getCart`);
};
export const removeItemFromCart = async (obj) => {
  return await axiosApiInstance.patch(`${serverUrl}/removeItemFromCart`, obj);
};

export const createWalletRecharge = async (obj) => {
  let userObj = await getDecodedToken();
  return await axios.post(
    `${serverUrl}/createWalletMoneyAdd/${userObj.userId}`,
    obj
  );
};

export const walletOrderPaymentCallback = async (obj, id) => {
  return axios.get(`${serverUrl}/paymentCB/${id}?${obj}`);
};
export const sendOtp = async (phone) => {
  console.log("sending otp");
  return axios.post(`${serverUrl}/generateSendOtp`, { phone });
};

export const forgotPasswordService = async (obj) => {
  console.log("sending otp");
  return axios.patch(`${serverUrl}/forgotPassword`, obj);
};

export const checkUserExists = async (obj) => {
  return axios.post(`${serverUrl}/checkUserExists`, obj);
};
